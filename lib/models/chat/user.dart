class Users {
  // String _id;
  String? phoneNumber;
  // String? passWord;
  String name;
  String? messageText;
  String imageURL;
  String? time;
  // bool online;
  // bool isActive;
  // String createAt;
  // String updateAt;
  bool? status;

  Users(
    {
      required this.name,
      this.messageText,
      required this.imageURL,
      this.time, 
      this.status,
      this.phoneNumber
    });
}