import 'dart:convert';

import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class SendNotification {

  Future<void> messageNotificationClassifier(ChatMessageTypes messageTypes,
      {String textMsg = "",
      required String connectionToken,
      required String currAccountUserName}) async {
    switch (messageTypes) {
      case ChatMessageTypes.None:
        break;
      case ChatMessageTypes.Text:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 tin nhắn~",
            body: textMsg);
        break;
      case ChatMessageTypes.Image:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 ảnh~",
            body: "");
        break;
      case ChatMessageTypes.Video:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 video~",
            body: "");
        break;
      case ChatMessageTypes.Document:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 tài liệu~",
            body: "");
        break;
      case ChatMessageTypes.Audio:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 bản ghi âm~",
            body: "");
        break;
      case ChatMessageTypes.Location:
        await sendNotification(
            token: connectionToken,
            title: "~$currAccountUserName đã gửi 1 vị trí~",
            body: "");
        break;
    }
  }

  Future<int> sendNotification(
      {required String token,
      required String title,
      required String body}) async {
    try {
      print("In Notification");

      final String _serverKey ="AAAAS9oLHKM:APA91bFuXldaCa0281-ovwB0xpItbc5IMy5RZATbMGKD49-qIqyLFU7mmlA6rL-yX70-_5UpjHdV507y5PTQcvcAjklI4YATYlp0QtMVO734eUufg924NqeL1NoXPr_XzA1f5imL_okR";

//Dùng này để stream tới http của firebase để gửi message
//API để đẩy thông báo lên firebase cho nó thông báo về
      final Response response = await http.post(
        Uri.parse("https://fcm.googleapis.com/fcm/send"),
        headers: <String, String>{
          "Content-Type": "application/json",
          "Authorization": "key=$_serverKey",
        },
        body: jsonEncode(<String, dynamic>{
          "notification": <String, dynamic>{
            "body": body,
            "title": title,
          },
          "priority": "high",
          "data": <String, dynamic>{
            "click": "FLUTTER_NOTIFICATION_CLICK",
            "id": "1",
            "status": "done",
            "collapse_key": "type_a",
          },
          "to": token,
        }),
      );

      print("Response from API is: ${response.statusCode}   ${response.body}");

      return response.statusCode;
    } catch (e) {
      print("Error in Notification Send: ${e.toString()}");
      return 404;
    }
  }
}
