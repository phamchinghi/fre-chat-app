class FirestoreFieldConstants{
   final String uid = "uid";
  final String userName = "user_name";
  final String password = "password";
  final String activities = "activity";
  final String connectionRequest = "connection_request";
  final String connection = "connections";
  final String createdDate = "created_date";
  final String createdTime = "created_time";
  final String phoneNumber = "phone_number";
  final String imageURL = "imageURL";
  final String token = "token";//token là mặc định phải có
  final String totalConnection = "total_connections";
  final String isActive = "is_active";
  final String status = "status";
}