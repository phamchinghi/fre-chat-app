import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class ForegroundNotificationManagement{

  final FlutterLocalNotificationsPlugin _notificationsPlugin = FlutterLocalNotificationsPlugin();

  final AndroidInitializationSettings _androidInitSetting = AndroidInitializationSettings("app_icon");

  ForegroundNotificationManagement(){
    final InitializationSettings _initSetting = InitializationSettings(android: _androidInitSetting);

    print('foreground noti');

    initAll(_initSetting);
  }

  initAll(InitializationSettings initializationSettings) async{
    final response = await _notificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (payload) async{
        print('on select Noti payload');
      });

      print('local noti status: $response');
  }

  Future<void> shownotification({required String title,required String body}) async{
    try{
      final AndroidNotificationDetails androidDetail = AndroidNotificationDetails(
        "CHANNEL ID","FRE CHAT APP",
        channelDescription: "A chat app made by NGHI AND TRIEU",
        importance: Importance.max);

        final NotificationDetails  generalNotification = NotificationDetails(android: androidDetail);
        await _notificationsPlugin.show(0, title, body, generalNotification, payload: title);
    }catch(e){
      print("Error show noti ${e.toString()}");
    }
  }

}