
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fre_chat_app/view/authentication/set_password.dart';

class AuthMethods{

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future verifyPhoneNumber(String phoneNumber, var _verificationId) async {
    _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (PhoneAuthCredential  phonesAuthCredentials) async {
        // await _auth.signInWithCredential(phonesAuthCredentials);
      },
      verificationFailed: (verificationFailed) async {
        print('Lỗi:' + verificationFailed.toString());
      },
      codeSent: (String verificationId,int? resendingToken) async {
          _verificationId = verificationId;
      },
      codeAutoRetrievalTimeout: (_verificationId) async {

      },
      timeout: const Duration(seconds: 60),
    );
  }

  Future<String> getCurrentUID() async {
    return await (_auth.currentUser!).uid;
  }

}