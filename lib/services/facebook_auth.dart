import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';

import '../home.dart';
import 'firestore_manage.dart';

class FacebookAuthentication{
  final FacebookAuth _facebookLogin = FacebookAuth.instance;
  final CloudFireStore _cloudFireStore = CloudFireStore();

  Future<FBSignInResults> facebookLogIn() async {
    try {
      if (await _facebookLogin.accessToken == null) {
        final LoginResult _fbLogInResult = await _facebookLogin.login();

        if (_fbLogInResult.status == LoginStatus.success) {
          final OAuthCredential _oAuthCredential =
          FacebookAuthProvider.credential(
              _fbLogInResult.accessToken!.token);

          if (FirebaseAuth.instance.currentUser != null)
            FirebaseAuth.instance.signOut();

          final UserCredential fbUser = await FirebaseAuth.instance
              .signInWithCredential(_oAuthCredential);

          print('Fb Log In Info: ${fbUser.user!.displayName}    ${fbUser.additionalUserInfo}');


          return FBSignInResults.SignInCompleted;
        }
        return FBSignInResults.UnExpectedError;
      } else {
        print('Already Fb Logged In');
        await logOut();
        return FBSignInResults.AlreadySignedIn;
      }
    } catch (e) {
      print('Facebook Log In Error: ${e.toString()}');
      return FBSignInResults.UnExpectedError;
    }
  }

  Future<UserCredential> getUserInfor() async {
        final LoginResult _fbLogInResult = await _facebookLogin.login();
        final UserCredential fbUser;
        final OAuthCredential _oAuthCredential =
        FacebookAuthProvider.credential(_fbLogInResult.accessToken!.token);

        fbUser = await FirebaseAuth.instance
            .signInWithCredential(_oAuthCredential);
        if (_fbLogInResult.status == LoginStatus.success) {
          if (FirebaseAuth.instance.currentUser != null)
            FirebaseAuth.instance.signOut();
        }
        print('Fb Log In Info: ${fbUser.user!.photoURL} ${fbUser.user!.phoneNumber}    ${fbUser
            .additionalUserInfo}');
        return fbUser;
  }

  Future<bool> logOut() async {
    try {
      print('Facebook Log Out');
      if (await _facebookLogin.accessToken != null) {
        await _facebookLogin.logOut();
        await FirebaseAuth.instance.signOut();
        return true;
      }
      return false;
    } catch (e) {
      print('Facebook Log out Error: ${e.toString()}');
      return false;
    }
  }
}