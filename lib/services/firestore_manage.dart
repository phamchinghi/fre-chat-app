import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fre_chat_app/global_use/constants.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/global_use/noti_management.dart';
import 'package:fre_chat_app/sqlite_management/local_database_management.dart';
import 'package:intl/intl.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class CloudFireStore{

  final _collectionName = 'users';

   final SendNotification _sendNotification = SendNotification();
  // final LocalDatabase _localDatabase = LocalDatabase();

  //check không cho giống sdt
  Future<bool> checkThisUserAlreadyPresentOrNot({required String phoneNumber}) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> findResults =
      await FirebaseFirestore.instance
          .collection(_collectionName)
          .where('phone_number', isEqualTo: phoneNumber)
          .get();

      print('Debug 1: ${findResults.docs.isEmpty}');

      return findResults.docs.isEmpty ? true : false;
    } catch (e) {
      print(
          'Lỗi kiểm tra user tồn tại: ${e.toString()}');
      return false;
    }
  }

  Future<bool> registerNewUser({

    required String userName,
    required String userId,
    required String phoneNumber,
    String? imgURL}) async {
    try {
      final String? _getToken = await FirebaseMessaging.instance.getToken();

      final String currentDate = DateFormat('dd-MM-yyyy').format(DateTime.now());

      final String currentTime = "${DateFormat('hh:mm a').format(DateTime.now())}";

      await FirebaseFirestore.instance.doc('$_collectionName/$userId').set({
        "uid":userId,
        "user_name": userName,
        "activity": [],
        "connection_request": [],
        "connections": {},
        "created_date": currentDate,
        "created_time": currentTime,
        "phone_number": phoneNumber,
        "imageURL": imgURL,
        "token": _getToken.toString(),      //token là mặc định phải có
        "total_connections": "",
        "is_active": true,
        "status":'Online',
      });
      print('Thêm dữ liệu vào firestore thành công');
      return true;

    } catch (e) {
      print('Lỗi đăng ký tài khoản: ${e.toString()}');
      return false;
    }
  }


  Future<Map<String, dynamic>> getTokenFromCloudStore(
      {required String userId}) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('$_collectionName/$userId')
          .get();

      print('DocumentSnapShot is: ${documentSnapshot.data()}');

      final Map<String, dynamic> importantData = Map<String, dynamic>();

      importantData["token"] = documentSnapshot.data()!["token"];
      importantData["date"] = documentSnapshot.data()!["created_date"];
      importantData["time"] = documentSnapshot.data()!["created_time"];

      return importantData;
    } catch (e) {
      print('Error in get Token from Cloud Store: ${e.toString()}');
      return {};
    }
  }

//Check xem user đã được ghi hay chưa
  Future<bool> userRecordPresentOrNot({required String userId}) async {
    try{
      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot = await FirebaseFirestore.instance.doc('${this._collectionName}/$userId').get();
      return documentSnapshot.exists;
    }catch(e){
      print('Lỗi ghi lại user: ${e.toString()}');
      return false;
    }
  }

  Future<bool> checkPhoneNumber(String phoneNumber) async{
    try{
      final QuerySnapshot<Map<String, dynamic>> findPhoneNumber = await FirebaseFirestore.instance
            .collection(_collectionName)
            .where('phone_number', isEqualTo: phoneNumber)
            .get();

     
      return findPhoneNumber.docs.single.exists ? true : false;
    }catch(e){
      print('Lỗi get phone: '+e.toString());
      return false;
    }
  }

  Future<void> logOut() async {
  await FirebaseAuth.instance.signOut();
  }


  Future<Stream<QuerySnapshot<Map<String,dynamic>>>?> fetchRealTimeDataFromFirebase() async{
    try{
      return FirebaseFirestore.instance
            .collection(this._collectionName)
            .snapshots();
    }catch(e){
      print("Lỗi lấy realtime data: $e");
      return null;
    }
  }

  Future<List<Map<String, dynamic>>> getAllUsersListExceptMyAccount({required String currentUser}) async {
    try {
      final QuerySnapshot<Map<String, dynamic>> querySnapshot =
      await FirebaseFirestore.instance
          .collection(_collectionName)
          .get();

      List<Map<String, dynamic>> _usersDataCollection = [];

      querySnapshot.docs.forEach(
              (QueryDocumentSnapshot<Map<String, dynamic>> queryDocumentSnapshot) {
            if (currentUser != queryDocumentSnapshot.id) {
              _usersDataCollection.add({
                queryDocumentSnapshot.id:
                '${queryDocumentSnapshot.get("user_name")}[user-name-about-divider]${queryDocumentSnapshot.get("phone_number")}[user-name-about-divider]${queryDocumentSnapshot.get("imageURL")}',
              });
            }
          });

      print(_usersDataCollection);

      return _usersDataCollection;
    } catch (e) {
      print('Lỗi get All Users List: ${e.toString()}');
      return [];
    }
  }

  Future<Map<String, dynamic>?> _getCurrentAccountAllData({required String userId}) async {
    try {
      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('${this._collectionName}/$userId')
          .get();

      return documentSnapshot.data();
    } catch (e) {
      print('Lỗi getCurrentAccountAll Data: ${e.toString()}');
      return {};
    }
  }
  
  Future<List<dynamic>> currentUserConnectionRequestList({required String userId}) async {
    try {
      Map<String, dynamic>? _currentUserData =await _getCurrentAccountAllData(userId: userId);

      final List<dynamic> _connectionRequestCollection = _currentUserData!["connection_request"];

      print('Collection: $_connectionRequestCollection');

      return _connectionRequestCollection;
    } catch (e) {
      print('Error in Current USer Collection List: ${e.toString()}');
      return [];
    }
  }

  Future<void> changeConnectionStatus({
    required String oppositeUserId,
    required String currentUserId,
    required String connectionUpdatedStatus,
    required List<dynamic> currentUserUpdatedConnectionRequest,
    bool storeDataAlsoInConnections = false,
  }) async {
    try {
      /// Opposite Connection database Update
      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('$_collectionName/$oppositeUserId')
          .get();

      Map<String, dynamic>? map = documentSnapshot.data();

      print('Map: $map');

      List<dynamic> _oppositeConnectionsRequestsList =
      map!["connection_request"];

      int index = -1;

      _oppositeConnectionsRequestsList.forEach((element) {
        if (element.keys.first.toString() == currentUserId) {
          index = _oppositeConnectionsRequestsList.indexOf(element);
        }
      });

      if (index > -1) _oppositeConnectionsRequestsList.removeAt(index);

      print('Opposite Connections: $_oppositeConnectionsRequestsList');

      _oppositeConnectionsRequestsList.add({
        currentUserId: connectionUpdatedStatus,
      });

      print('Opposite Connections: $_oppositeConnectionsRequestsList');

      map["connection_request"] = _oppositeConnectionsRequestsList;

      if (storeDataAlsoInConnections) {
        map[FirestoreFieldConstants().connection].addAll({
          currentUserId: [],
        });
      }

      await FirebaseFirestore.instance
          .doc('$_collectionName/$oppositeUserId')
          .update(map);

      /// Current User Connection Database Update
      final Map<String, dynamic>? currentUserMap = await _getCurrentAccountAllData(userId: currentUserId);

      currentUserMap!["connection_request"] = currentUserUpdatedConnectionRequest;

      if (storeDataAlsoInConnections) {
        currentUserMap[FirestoreFieldConstants().connection].addAll({
          oppositeUserId: [],
        });
      }

      await FirebaseFirestore.instance
          .doc('$_collectionName/$currentUserId')
          .update(currentUserMap);
    } catch (e) {
      print('Error in Change Connection Status: ${e.toString()}');
    }
  }

  ///////////////////////////////////////////////////////////////////
  Future<Stream<QuerySnapshot<Map<String, dynamic>>>?> fetchRealTimeDataFromFirestore() async {
    try {
      return FirebaseFirestore.instance
          .collection(this._collectionName)
          .snapshots();
    } catch (e) {
      print('Lỗi không tìm được dữ liệu thời gian thực : ${e.toString()}');
      return null;
    }
  }
  Future<Stream<DocumentSnapshot<Map<String, dynamic>>>?> fetchRealTimeMessages() async {
    try {
      return FirebaseFirestore.instance
          .doc(
          '$_collectionName/${FirebaseAuth.instance.currentUser!.uid.toString()}')
          .snapshots();
    } catch (e) {
      print('Error in Fetch Real Time Data : ${e.toString()}');
      return null;
    }
  }

  Future<void> removeOldMessages({required String connectionEmail}) async {
    try {
      final String? currentUserId = FirebaseAuth.instance.currentUser!.uid;

      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc("${this._collectionName}/$currentUserId")
          .get();

      final Map<String, dynamic>? connectedUserData = documentSnapshot.data();

      connectedUserData![FirestoreFieldConstants().connection]
      [connectionEmail.toString()] = [];

      print(
          "After Remove: ${connectedUserData[FirestoreFieldConstants().connection]}");

      await FirebaseFirestore.instance
          .doc("${this._collectionName}/$currentUserId")
          .update({
        FirestoreFieldConstants().connection:
        connectedUserData[FirestoreFieldConstants().connection],
      }).whenComplete(() {
        print('Data Deletion Completed');
      });
    } catch (e) {
      print('Error in Send Data: ${e.toString()}');
    }
  }

  Future<String?> uploadMediaToStorage(File filePath,
      {required String reference}) async {
    try {
      String? downLoadUrl;

      final String fileName =
          '${FirebaseAuth.instance.currentUser!.uid}${DateTime.now().day}${DateTime.now().month}${DateTime.now().year}${DateTime.now().hour}${DateTime.now().minute}${DateTime.now().second}${DateTime.now().millisecond}';

      final Reference firebaseStorageRef = FirebaseStorage.instance.ref(reference).child(fileName);

      print('Firebase Storage Reference: $firebaseStorageRef');

      final UploadTask uploadTask = firebaseStorageRef.putFile(filePath);

      await uploadTask.whenComplete(() async {
        print("Media Uploaded");
        downLoadUrl = await firebaseStorageRef.getDownloadURL();
        print("Download Url: $downLoadUrl}");
      });

      return downLoadUrl!;
    } catch (e) {
      print("Error: Firebase Storage Exception is: ${e.toString()}");
      return null;
    }
  }

  Future<void> sendMessageToConnection({required String connectionUserName,required Map<String, Map<String, String>> sendMessageData,required ChatMessageTypes chatMessageTypes}) async {
    try {
      final LocalDatabase _localDatabase = LocalDatabase();

      final String? currentUserId = FirebaseAuth.instance.currentUser!.uid;

      final String? _getConnectedUserId =await _localDatabase.getParticularFieldDataFromImportantTable(
          userName: connectionUserName,
          getField: GetFieldForImportantDataLocalDatabase.UserId);

      final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc("${_collectionName}/$_getConnectedUserId")
          .get();

      final Map<String, dynamic>? connectedUserData = documentSnapshot.data();

      List<dynamic>? getOldMessages = connectedUserData![FirestoreFieldConstants().connection][currentUserId.toString()];
      if (getOldMessages == null) getOldMessages = [];

      getOldMessages.add(sendMessageData);

      connectedUserData[FirestoreFieldConstants().connection]
      [currentUserId.toString()] = getOldMessages;

      print("Data checking: ${connectedUserData[FirestoreFieldConstants().connection]}");

      await FirebaseFirestore.instance
          .doc("${this._collectionName}/$_getConnectedUserId")
          .update({
        FirestoreFieldConstants().connection:
        connectedUserData[FirestoreFieldConstants().connection],
      }).whenComplete(() async {
        print('Data Send Completed');

        final String? connectionToken = await _localDatabase.getParticularFieldDataFromImportantTable(
            userName: connectionUserName,
            getField: GetFieldForImportantDataLocalDatabase.Token);

        final String? currentAccountUserName = await _localDatabase.getUserNameForCurrentUser(
            FirebaseAuth.instance.currentUser!.uid.toString());

        await _sendNotification.messageNotificationClassifier(chatMessageTypes,
            connectionToken: connectionToken ?? "",
            currAccountUserName: currentAccountUserName ?? "");
      });
    } catch (e) {
      print('error in Send Data: ${e.toString()}');
    }
  }


}