import 'package:flutter/material.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:fre_chat_app/widgets/myheaderbar.dart';

import 'change_password.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({ Key? key }) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: 'Quên mật khẩu',
        bottomLeft: 0,
        bottomRight: 0,
        iconLeading: Icons.arrow_back,
        ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget> [
            const MyHeaderBar(icon:Icons.lock_open_outlined),
            const Padding(padding: EdgeInsets.only(top: 50)),
            const Text('Chúng tôi đã gửi mã đến SDT(0123456789) đã đăng kí',
              style: TextStyle(fontSize: 12,color: Colors.black54),

            ),
            const Padding(padding: EdgeInsets.only(top: 20)),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 50,
              ),
              child: const TextField(
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                        Radius.circular(30)
                    ),
                  ),
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Số điện thoại',
                ),
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 10)),
            const Text('Không tìm thấy SDT hoặc gì gì đó',
              style: TextStyle(fontSize: 12,color: Colors.redAccent),
            ),
            const Padding(padding: EdgeInsets.only(top: 30)),
            ButtonTheme(
              height: 40,
              minWidth: 100,
              child: RaisedButton(
                onPressed: () {
                 Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const ChangePassword()),
                  );
                },
                child: const Text(
                  "Xác thực",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}