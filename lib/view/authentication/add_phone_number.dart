import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

import '../../home.dart';

class AddPhoneFacebook extends StatefulWidget {
  const AddPhoneFacebook({Key? key, required this.userCredential,}) : super(key: key);
  final UserCredential userCredential;
  @override
  _AddPhoneFacebookState createState() => _AddPhoneFacebookState(userCredential);
}

class _AddPhoneFacebookState extends State<AddPhoneFacebook> {
  
  bool isLoading = false;
  
  String phoneNumber = "";
  TextEditingController phoneNumberTextEditing = new TextEditingController();
  final CloudFireStore _cloudFireStore = CloudFireStore();

  UserCredential _userCredential;

  _AddPhoneFacebookState(this._userCredential);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: _userCredential.user!.displayName.toString(),
        bottomLeft: 0,
        bottomRight: 0,
        iconLeading: Icons.arrow_back,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget> [
            Container(
              height: 200,
              decoration: BoxDecoration(
                borderRadius:BorderRadius.only(
                    bottomRight :Radius.circular(50),
                    bottomLeft :Radius.circular(50)
                ),
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.pin_outlined,size: 96,color: Colors.white,)
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top:50)),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: const Text(
              'Vì tài khoản của bạn không có SDT nên bạn phải thêm SDT để có thể tìm thấy bạn!',
              style: TextStyle(
                color: Colors.redAccent,
                fontWeight: FontWeight.bold
              )
            ),
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            Container(
              padding: const EdgeInsets.symmetric(
                  horizontal: 50
              ),
              child: TextField(
                maxLength: 10,
                controller: phoneNumberTextEditing,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(30)
                      )
                  ),
                  labelText: 'Số điện thoại',
                  contentPadding: EdgeInsets.all(10),
                ),

              ),
            ),

            const Padding(padding: EdgeInsets.only(top: 50)),
            ButtonTheme(
              height: 40,
              minWidth: 100,
              child: RaisedButton(
                onPressed: () async {
                  String locate = "+84";
                  phoneNumber = phoneNumberTextEditing.text.substring(1);
                  phoneNumber = locate + phoneNumber;
                  print(phoneNumber);
                    final bool phoneNumberCheck = await _cloudFireStore.checkThisUserAlreadyPresentOrNot(phoneNumber: this.phoneNumber);
                    if(!phoneNumberCheck){
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content:Text("Số điện thoại đã được đăng ký",
                                    style: TextStyle(color:Colors.red,fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,),
                            backgroundColor: Colors.white,
                        ));
                    }else{
                      final bool _registerUserCheck = await _cloudFireStore
                          .registerNewUser(
                          userName: _userCredential.user!.displayName.toString(),
                          userId: _userCredential.user!.uid,
                          phoneNumber:this.phoneNumber,
                          imgURL: _userCredential.user!.photoURL.toString(),
                          );
                        if(isLoading) return;

                        setState(() =>this.isLoading = true);
                        await Future.delayed(Duration(seconds: 2));
                        setState(() =>this.isLoading = false);
                      if(_registerUserCheck){
                        print('Tạo tài khoản thành công');
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Home(),));
                      }else{
                        print('Tạo tài khoản không thành công');
                      }
                    }
                },
                child: isLoading
                    ? SizedBox( child: CircularProgressIndicator(color: Colors.white,strokeWidth: 2,), width: 20,height: 20,)
                    : Text("Đồng ý",style: TextStyle(color: Colors.white,)),
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
