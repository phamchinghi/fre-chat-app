import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/home.dart';
import 'package:fre_chat_app/services/facebook_auth.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/sqlite_management/local_database_management.dart';
import 'package:fre_chat_app/view/authentication/add_phone_number.dart';
import 'package:fre_chat_app/view/authentication/forgot_password.dart';
import 'dart:convert';

import 'package:fre_chat_app/view/authentication/signup.dart';
import 'package:fre_chat_app/view/authentication/verifying.dart';
import 'package:loading_overlay/loading_overlay.dart';
class Login extends StatefulWidget {
  const Login({ Key? key }) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}


class _LoginState extends State<Login> {
  bool _isObscure = true;
  bool _isLoading = false;
  bool _isPhoneNumber = false;
  bool isLoading = false;
    bool isPhoneNumberError = false;

  final LocalDatabase _localDatabase = LocalDatabase();

  final TextEditingController phoneNumberEdittingController = TextEditingController();

  final FacebookAuthentication _facebookAuthentication = FacebookAuthentication();
  final CloudFireStore _cloudFireStore = CloudFireStore();
  final FacebookAuth _facebookLogin = FacebookAuth.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;


  @override
  Widget build(BuildContext context) {
    // _facebookLogin.
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Image.asset('assets/images/app_icon.png',
            height: 200,
            width: 200
        ),
        centerTitle: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(200),
          ),
        ),
        toolbarHeight: 200,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(top: 50),
        child:  Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 50,
                  vertical: 10,
                ),
                child: TextField(
                  keyboardType: TextInputType.number,
                  maxLength: 10,
                  controller: phoneNumberEdittingController,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(30)
                      ),
                    ),
                    contentPadding: EdgeInsets.all(10),
                    labelText: 'Số điện thoại',
                    errorText: isPhoneNumberError ? 'Số điện thoại không hợp lệ' : null,
                  ),
                  
                ),
              ),
              SizedBox(
                child: isLoading
                    ? SizedBox( child: CircularProgressIndicator(color: Colors.black,strokeWidth: 2,), width: 20,height: 20,)
                    : null,
              ),
              const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10)
              ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20)
                  ),
              ButtonTheme(
                height: 40,
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)
                  ),
                  onPressed: () async {
                    if(phoneNumberEdittingController.text.isEmpty){
                      setState(() {
                        isPhoneNumberError = true;
                      });
                    }else if(phoneNumberEdittingController.text.length != 10){
                      setState(() {
                        isPhoneNumberError = true;
                      });
                    }else{
                      setState(() {
                        isPhoneNumberError = false;
                      });
                      String phoneNumber = phoneNumberEdittingController.text.substring(1);
                      if(isLoading) return;

                      setState(() =>this.isLoading = true);
                      await Future.delayed(Duration(seconds: 2));
                      setState(() =>this.isLoading = false);

                      final bool phoneNumberCheck = await _cloudFireStore.checkThisUserAlreadyPresentOrNot(phoneNumber:"+84" + phoneNumber);

                      if(!phoneNumberCheck){
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content:Text("Số điện thoại đã được đăng ký",
                                    style: TextStyle(color:Colors.red,fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,),
                            backgroundColor: Colors.white,
                        ));
                        setState(() =>this.isLoading = false);
                      }else{
                        await Future.delayed(Duration(seconds: 2));
                        setState(() =>this.isLoading = false);
                        Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Verifying(number:'+84' + phoneNumber))
                        );
                      };
                    }
                  },
                  child: const Text(
                    "Tiếp tục",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ), 
                ), 
              ),
                  // ButtonTheme(
                  //   height: 40,
                  //   minWidth: 100,
                  //   child: RaisedButton(
                  //     onPressed: () async {
                  //       String locate = "+84";
                  //       String phoneNumber; //= _phoneNumberController.text.substring(1);
                  //       String password; //=  _passwordController.text.trim();

                  //       if(_phoneNumberController.text.length == 10 && _passwordController.text.isNotEmpty){
                  //         setState(() {
                  //           _isPhoneNumber = false;
                  //           _isPassword = false;
                  //           isLoading = true;
                  //         });

                  //         phoneNumber = _phoneNumberController.text.substring(1);
                  //         password = _passwordController.text.trim();
                  //         phoneNumber = locate + phoneNumber;

                  //         final bool checkPhone = await _cloudFireStore.checkPhoneNumber(phoneNumber);
                  //         final bool checkPassword = await _cloudFireStore.checkPassword(password);


                  //         // String userId =  FirebaseAuth.instance.currentUser!.uid;
                  //         // print(FirebaseFirestore.instance.collection('users').doc(userId).toString());

                  //         // if(checkPhone && checkPassword){
                  //         //   await Future.delayed(Duration(seconds: 2));
                  //         //   setState(() {
                  //         //     isLoading = false;
                  //         //   });
                  //         //   print('Đúng tài khoản mật khẩu');
                  //         //   Navigator.push(
                  //         //       context,
                  //         //       MaterialPageRoute(builder: (context) => Home()));
                  //         // }else{
                  //         //   setState(() {
                  //         //     _isPhoneNumber = true;
                  //         //     isLoading = false;
                  //         //   });
                  //         // }
                  //       } else if(_phoneNumberController.text.length !=10 && _passwordController.text.isNotEmpty ){   //check sdt không = 10
                  //         setState(() {
                  //           _isPhoneNumber = true;
                  //           _isPassword = false;
                  //         });
                  //       } else if(_phoneNumberController.text.length == 10 && _passwordController.text.isEmpty ){     //check
                  //         setState(() {
                  //           _isPhoneNumber = false;
                  //           _isPassword = true;
                  //         });
                  //       }
                  //       // check chổ này chưa được nè pass rỗng và sdt rỗng
                  //       else if(_phoneNumberController.text.isEmpty && _passwordController.text.isEmpty ){
                  //         setState(() {
                  //           _isPhoneNumber = true;
                  //           _isPassword = true;
                  //         });
                  //       }else if(_phoneNumberController.text.length !=10 && _passwordController.text.isEmpty ){
                  //         setState(() {
                  //           _isPhoneNumber = true;
                  //         });
                  //       }
                  //     },
                  //     child: const Text(
                  //       "Đăng nhập",
                  //       style: TextStyle(
                  //         color: Colors.white,
                  //       ),
                  //     ),
                  //     color: Theme.of(context).primaryColor,
                  //     shape: RoundedRectangleBorder(
                  //         borderRadius: BorderRadius.circular(20)
                  //     ),
                  //   ),
                  // ),
              // FlatButton(
              //   onPressed: (){

                  

              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(builder: (context) => const ForgotPassword()),
              //     );
              //   },
              //   textColor: Theme.of(context).primaryColor,
              //   child: Text('Quên mật khẩu?'),
              // ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 10)
              ),
              Text('hoặc'),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 10)
              ),
              Container(
                height: 40,
                child: RaisedButton.icon(
                  splashColor: Colors.deepPurple,
                  color: Colors.white,
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  onPressed: () async {
                    print("Đăng nhập facebook");

                    if (mounted) {
                      setState(() {
                        this._isLoading = true;
                      });
                    }

                    // final FBSignInResults _fbSignInResults =
                    // await this._facebookAuthentication.facebookLogIn();

                    // String msg = '';
                    // if (_fbSignInResults == FBSignInResults.SignInCompleted) {
                    //   Navigator.push(
                    //         context,
                    //         MaterialPageRoute(builder: (context) => Home(),));
                    //   msg = 'Sign In Completed';
                    // } else if (_fbSignInResults == FBSignInResults.SignInNotCompleted)
                    //   msg = 'Sign In not Completed';
                    // else if (_fbSignInResults == FBSignInResults.AlreadySignedIn)
                    //   msg = 'Already Google SignedIn';
                    // else
                    //   msg = 'Unexpected Error Happen';
                    
                    // ScaffoldMessenger.of(context)
                    //     .showSnackBar(SnackBar(content: Text(msg)));

                    // if (_fbSignInResults == FBSignInResults.SignInCompleted){
                    final UserCredential user = await _facebookAuthentication.getUserInfor();
                    if(user.user!.phoneNumber == null){
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddPhoneFacebook(userCredential: user),));
                    }else{
                      final bool _registerUserCheck = await _cloudFireStore
                          .registerNewUser(
                        userName: user.user!.displayName.toString(),
                        userId: user.user!.uid,
                        phoneNumber: user.user!.phoneNumber.toString(),
                        imgURL: ""
                      );
                      if(_registerUserCheck){
                        print('Tạo tài khoản thành công');
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(builder: (_) => Home()),
                                (route) => false);
                      }else{
                        print('Tạo tài khoản không thành công');
                      }
                    }
                    // }
                    if (mounted) {
                      setState(() {
                        this._isLoading = false;
                      });
                    }
                  },
                  icon: const Icon(
                    Icons.facebook,
                    color: Colors.blue,
                  ),
                  label: Text("Đăng nhập với facebook",style: TextStyle(fontSize: 18),),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
