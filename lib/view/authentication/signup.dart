import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/view/authentication/verifying.dart';

import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:fre_chat_app/widgets/myheaderbar.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignUp extends StatefulWidget {
  const SignUp({ Key? key }) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController phoneNumberEdittingController = new TextEditingController();

  bool isLoading = false;
  bool isPhoneNumberError = false;
  final CloudFireStore _cloudFireStore = CloudFireStore();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        iconLeading: Icons.arrow_back,
        bottomRight: 0,
        bottomLeft: 0,
        title: '',
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const MyHeaderBar(icon:Icons.phone),
            const Padding(padding: EdgeInsets.only(top: 50)),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 50,
              ),
              child: TextField(
                onChanged: (value){
                  if(value.length > 0){
                     setState(() {
                      isPhoneNumberError = false;
                    });
                  }
                },
                controller: phoneNumberEdittingController,
                maxLength: 10,
                keyboardType: TextInputType.number,
                style: const TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                ),
                decoration: InputDecoration(
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(30)
                      ),
                    ),
                    contentPadding: EdgeInsets.all(10),
                    labelText: 'Số điện thoại',
                    prefixText: '+84 - ',
                    errorText: isPhoneNumberError ? 'Vui lòng nhập đầy đủ số điện thoại' : null,
                ),
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 50)),
            Container(
              height: 40,
              padding: EdgeInsets.symmetric(horizontal: 110),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  textStyle: TextStyle(fontSize: 18),
                  minimumSize: Size.fromHeight(40),
                  shape: StadiumBorder(),
                  padding: EdgeInsets.symmetric(horizontal: 50)
                ),
                child: isLoading
                    ? SizedBox( child: CircularProgressIndicator(color: Colors.white,strokeWidth: 2,), width: 20,height: 20,)
                    : Text("Đăng ký",style: TextStyle(color: Colors.white,)),
                onPressed: () async {
                  if(phoneNumberEdittingController.text.isEmpty){
                    setState(() {
                      isPhoneNumberError = true;
                    });
                  }else if(phoneNumberEdittingController.text.length != 10){
                    setState(() {
                      isPhoneNumberError = true;
                    });
                  }else{
                    setState(() {
                      isPhoneNumberError = false;
                    });
                    String phoneNumber = phoneNumberEdittingController.text.substring(1);
                    if(isLoading) return;

                    setState(() =>this.isLoading = true);
                    await Future.delayed(Duration(seconds: 2));
                    setState(() =>this.isLoading = true);

                    final bool phoneNumberCheck = await _cloudFireStore.checkThisUserAlreadyPresentOrNot(phoneNumber:"+84" + phoneNumber);

                    if(!phoneNumberCheck){
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content:Text("Số điện thoại đã được đăng ký",
                                  style: TextStyle(color:Colors.red,fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,),
                          backgroundColor: Colors.white,
                      ));
                      setState(() =>this.isLoading = false);
                    }else{
                      Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Verifying(number:'+84' + phoneNumber))
                      );
                    };
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
