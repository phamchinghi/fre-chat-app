import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/services/auth.dart';
import 'package:fre_chat_app/view/authentication/set_password.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:fre_chat_app/widgets/myheaderbar.dart';

class Verifying extends StatefulWidget {
  const Verifying({Key? key, this.number}) : super(key: key);
  final number;
  @override
  _VerifyingState createState() => _VerifyingState(number);
}

class _VerifyingState extends State<Verifying> with TickerProviderStateMixin {
  final phoneNumber;
  var _status = Status.Waiting;
  var _verificationId;
  var id;
  var _textEditingController = TextEditingController();

  static const maxSeconds = 60;
  int _seconds = maxSeconds;
  Timer? _timer;



  _VerifyingState(this.phoneNumber);

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final AuthMethods _authMethods = new AuthMethods();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _verifyPhoneNumber();
    startTimer();
  }
// ------------Verifying phone number------------
  Future _verifyPhoneNumber() async {
    _auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: (PhoneAuthCredential  phonesAuthCredentials) async {
        await _auth.signInWithCredential(phonesAuthCredentials);

      },
      verificationFailed: (verificationFailed) async {
        print('Lỗi:' + verificationFailed.toString());
      },
      codeSent: (verificationId,int? resendingToken) async {
        setState(() {
          this._verificationId = verificationId;
        });
      },
      codeAutoRetrievalTimeout: (_verificationId) async {

      },
      timeout: const Duration(seconds: 60),
    );
  }
  Future _sendCodeToFirebase({String? code}) async {
    if(this._verificationId != null){
      var credential = PhoneAuthProvider.credential(
          verificationId: _verificationId, smsCode: code!);

      await _auth
          .signInWithCredential(credential)
          .then((value){
        Navigator.push(context,
            MaterialPageRoute(
                builder: (context)=>SetPassword(userId: _auth.currentUser!.uid,phoneNumber: phoneNumber,)));
      } )
          .whenComplete(() {} )
          .onError((error, stackTrace){
        setState(() {
          _textEditingController.text = "";
          this._status = Status.Error;
        });
      });
    }
  }
// ------------Verifying phone number------------

//----------------------Countdown-------------------------
  void startTimer(){
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_seconds == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _seconds--;
          });
        }
      },
    );
  }
//----------------------Countdown-------------------------
  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(
          iconLeading: Icons.arrow_back,
          bottomRight: 0,
          bottomLeft: 0,
          title: '',
        ),
        body: SingleChildScrollView(
            child: _status != Status.Error ?
            Column(
                children: [
                  const MyHeaderBar(icon: Icons.verified_user_outlined,),
                  const Padding(padding: EdgeInsets.only(top: 50)),
                  Text('Nhập mã OTP gồm 6 số mà chúng tôi đã gửi đến SDT',
                    style: TextStyle(fontSize: 12,color: Colors.black38),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Text(phoneNumber == null ? "" : phoneNumber,style: TextStyle(color: Colors.green,fontSize: 18)),
                  const Padding(padding: EdgeInsets.only(top: 20)),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 50,
                    ),
                    child: TextField(
                        onChanged: (value){
                          print(value);
                          if(value.length == 6){
                            // Thực hiện việc verify
                            _sendCodeToFirebase(code: value);
                            
                          }
                        },
                        maxLength: 6,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          letterSpacing: 30,
                          fontSize: 16,
                          color: Colors.black,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(30)
                            ),
                          ),
                          contentPadding: EdgeInsets.all(10),
                        ),
                        controller: _textEditingController,
                        keyboardType: TextInputType.number,
                        autofillHints: <String>[AutofillHints.telephoneNumber]
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 20)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('$_seconds s'),
                    ],
                  ),
                  const Padding(padding: EdgeInsets.only(top: 10)),
                  ButtonTheme(child:
                  TextButton(
                      onPressed:() async {
                        _authMethods.verifyPhoneNumber(phoneNumber,_verificationId);
                        // _verifyPhoneNumber();
                        startTimer();
                      },
                      child: const Text('Gửi lại')
                  )
                  ),
                  const Padding(padding: EdgeInsets.only(top: 30)),
                ]
            )
                :
            Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const MyHeaderBar(icon: Icons.verified_user_outlined,),
                  const Padding(padding: EdgeInsets.only(top: 50)),
                  Text('Sai mã OTP vui lòng nhập lại',
                    style: TextStyle(fontSize: 16,color: Colors.red),
                  ),
                  Padding(padding: EdgeInsets.only(top: 10)),
                  Text(phoneNumber == null ? "" : phoneNumber,style: TextStyle(color: Colors.green,fontSize: 18)),
                  const Padding(padding: EdgeInsets.only(top: 20)),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 50,
                    ),
                    child: TextField(
                        onChanged: (value){
                          print(value);
                          if(value.length == 6){
                            // Thực hiện việc verify
                            _sendCodeToFirebase(code: value);
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(builder: (context) => const SetPassword(),));
                          }
                        },
                        maxLength: 6,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          letterSpacing: 30,
                          fontSize: 16,
                          color: Colors.black,
                        ),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(30)
                            ),
                          ),
                          contentPadding: EdgeInsets.all(10),
                        ),
                        controller: _textEditingController,
                        keyboardType: TextInputType.number,
                        autofillHints: <String>[AutofillHints.telephoneNumber]
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 20)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('$_seconds s'),
                    ],
                  ),
                  const Padding(padding: EdgeInsets.only(top: 10)),
                  ButtonTheme(child:
                  TextButton(
                      onPressed:() async {
                        _authMethods.verifyPhoneNumber(phoneNumber,_verificationId);
                        // _verifyPhoneNumber();
                        startTimer();
                      },
                      child: const Text('Gửi lại')
                  )
                  ),
                  const Padding(padding: EdgeInsets.only(top: 30)),
                ]
            )
        )
    );
  }
}

