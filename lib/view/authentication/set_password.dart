import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/sqlite_management/local_database_management.dart';
import 'package:fre_chat_app/view/authentication/login.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:fre_chat_app/widgets/myheaderbar.dart';

import '../../home.dart';

class SetPassword extends StatefulWidget {
  const SetPassword({ Key? key, this.userId, this.phoneNumber }) : super(key: key);
  final userId, phoneNumber;
  @override
  _SetPasswordState createState() => _SetPasswordState(userId,phoneNumber);
}


class _SetPasswordState extends State<SetPassword> {
  bool _isObscure = true;
  bool _isObscureConfirm = true;
  String userName = "";
  String password = "";
  final String userID,phoneNumber;
  final LocalDatabase _localDatabase = LocalDatabase();
  TextEditingController userNameTextEditing = new TextEditingController();
  TextEditingController passwordTextEditing = new TextEditingController();
  TextEditingController confirmPasswordTextEditing = new TextEditingController();

  final CloudFireStore _cloudFireStore = CloudFireStore();

  _SetPasswordState(this.userID, this.phoneNumber);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: 'Thiết lập tài khoản',
        bottomLeft: 0,
        bottomRight: 0,
        iconLeading: Icons.arrow_back,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget> [
            Container(
              height: 200,
              decoration: BoxDecoration(
                borderRadius:BorderRadius.only(
                    bottomRight :Radius.circular(50),
                    bottomLeft :Radius.circular(50)
                ),
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.pin_outlined,size: 96,color: Colors.white,)
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top:50)),
            Container(
              padding: const EdgeInsets.symmetric(
                  horizontal: 50
              ),
              child: TextField(
                controller: userNameTextEditing,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black
                ),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                          Radius.circular(30)
                      )
                  ),
                  labelText: 'Tên hiển thị',
                  contentPadding: EdgeInsets.all(10),
                ),

              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 50)),
            ButtonTheme(
              height: 40,
              minWidth: 100,
              child: RaisedButton(
                onPressed: () async {
                  userName = userNameTextEditing.text.trim();
                  print(userName);
                  password = confirmPasswordTextEditing.text;
                  print(userID + userName + password + phoneNumber);
                  final bool phoneNumberCheck = await _cloudFireStore.checkThisUserAlreadyPresentOrNot(phoneNumber: this.phoneNumber);
                  if(!phoneNumberCheck){
                    ScaffoldMessenger
                        .of(context)
                        .showSnackBar(SnackBar(content: Text("Số điện thoại đã được đăng ký")));
                  }else{
                    final bool _registerUserCheck =
                      await _cloudFireStore.registerNewUser(
                          userName: userName, userId: userID, phoneNumber: phoneNumber,imgURL: "");
                    if(_registerUserCheck){
                      print('Tạo tài khoản thành công');
                      /// Calling Local Databases Methods To Intitialize Local Database with required MEthods
                      await _localDatabase.createTableToStoreImportantData();

                      final Map<String,dynamic> _importantFetchedData =
                        await _cloudFireStore.getTokenFromCloudStore(userId: FirebaseAuth.instance.currentUser!.uid.toString());

                      await _localDatabase.insertOrUpdateDataForThisAccount(
                          userName: userNameTextEditing.text,
                          userPhone: FirebaseAuth.instance.currentUser!.uid.toString(),
                          userToken: _importantFetchedData["token"],
                          userId: userID,
                          userAccCreationDate: _importantFetchedData["date"],
                          userAccCreationTime: _importantFetchedData["time"]);

                      await _localDatabase
                          .createTableForUserActivity(tableName: userID);

                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Home(),));
                    }else{
                      print('Tạo tài khoản không thành công');
                    }
                  }
                },
                child: const Text(
                  "Đồng ý",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}