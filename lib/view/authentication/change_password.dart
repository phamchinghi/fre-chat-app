import 'package:flutter/material.dart';
import 'package:fre_chat_app/view/authentication/login.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:fre_chat_app/widgets/myheaderbar.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({ Key? key }) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {

  bool _isObscure = true;
  bool _isObscureConfirm = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: 'Đổi mật khẩu', 
        bottomLeft: 0,
        bottomRight: 0,
        iconLeading: Icons.arrow_back,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget> [
            Container(
              height: 200,
              decoration: BoxDecoration(
                borderRadius:BorderRadius.only(
                  bottomRight :Radius.circular(50),
                  bottomLeft :Radius.circular(50)
                ),
                color: Theme.of(context).primaryColor,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.pin_outlined,size: 96,color: Colors.white,)
                ],
              ),
            ),
            const Padding(padding: EdgeInsets.only(top:50)),
            Container(
              padding: const EdgeInsets.symmetric(
                  horizontal: 50
              ),
              child: TextField(
                obscureText: _isObscure,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black
                ),
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(30)
                        )
                    ),
                    labelText: 'Mật khẩu',
                    contentPadding: EdgeInsets.all(10),
                    suffixIcon: IconButton(
                      icon: Icon(
                        _isObscure ? Icons.visibility : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                    ),
                ),

              ),
            ),
            const Padding(padding: EdgeInsets.only(top:10)),
            Container(
              padding: const EdgeInsets.symmetric(
                  horizontal: 50
              ),
              child: TextField(
                  obscureText: _isObscureConfirm,
                  style: const TextStyle(
                      fontSize: 16,
                      color: Colors.black
                  ),
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(30)
                        )
                    ),
                    labelText: 'Xác nhận mật khẩu',
                    contentPadding: EdgeInsets.all(10),
                     suffixIcon: IconButton(
                      icon: Icon(
                        _isObscureConfirm ? Icons.visibility : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscureConfirm = !_isObscureConfirm;
                        });
                      },
                    ),
                  )
              ),
            ),
            const Padding(padding: EdgeInsets.only(top: 50)),
            ButtonTheme(
              height: 40,
              minWidth: 100,
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Login(),));
                },
                child: const Text(
                  "Xác thực",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}