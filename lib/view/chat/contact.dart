import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/view/chat/setting/account_setting.dart';
import 'package:loading_overlay/loading_overlay.dart';

class Contact extends StatefulWidget {
  const Contact({Key? key}) : super(key: key);


  @override
  _ContactState createState() => _ContactState();

  Size get preferredSize => const Size(40.0, 60.0);
}



class _ContactState extends State<Contact> {
//////////////////////////////////////////////////////
  List<Map<String, dynamic>> _availableUsers = [];
  List<Map<String, dynamic>> _sortedAvailableUsers = [];
  List<dynamic> _myConnectionRequestCollection = [];

  bool _isLoading = false;

  final CloudFireStore _cloudStoreDataManagement = CloudFireStore();

    Icon searchIcon = const Icon(Icons.search_rounded);
  Widget titleAppbar = const Text('');

  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(3, (i) => "Ly Van Trieu $i");
  var items = <String>['a','b','c','d','e','f','g','h','i'];

  String imgURL = "";
  String userPhone = "";
  String userName = "";

  Future<void> _initialDataFetchAndCheckUp() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }

    final List<Map<String, dynamic>> takeUsers =
    await _cloudStoreDataManagement.getAllUsersListExceptMyAccount(
        currentUser: FirebaseAuth.instance.currentUser!.uid.toString());

    final List<Map<String, dynamic>> takeUsersAfterSorted = [];

    if (mounted) {
      setState(() {
        takeUsers.forEach((element) {
          if (mounted) {
            setState(() {
              takeUsersAfterSorted.add(element);
            });
          }
        });
      });
    }

    final List<dynamic> _connectionRequestList =
    await _cloudStoreDataManagement.currentUserConnectionRequestList(
        userId: FirebaseAuth.instance.currentUser!.uid.toString());

    if (mounted) {
      setState(() {
        _availableUsers = takeUsers;
        _sortedAvailableUsers = takeUsersAfterSorted;
        _myConnectionRequestCollection = _connectionRequestList;
      });
    }

    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _initialDataFetchAndCheckUp();
    super.initState();
     getCurrentImgURL(FirebaseAuth.instance.currentUser!.uid);
    getCurrentUserPhone(FirebaseAuth.instance.currentUser!.uid);
    getCurrentUserName(FirebaseAuth.instance.currentUser!.uid);
  }
  ////////////////////////////////////////////////////////////////////////////////

getCurrentImgURL(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    imgURL = documentSnapshot.get('imageURL').toString();
    print("img URL: "+imgURL);
  }
  getCurrentUserPhone(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    userPhone ="0" + documentSnapshot.get('phone_number').toString().substring(3,12);
    print("Phone number: "+userPhone);
  }
  getCurrentUserName(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    userName =documentSnapshot.get('user_name').toString();
    print("User name: "+userName);
  }

  void filterSearchResults(String query) {
    List<String> dummySearchList = <String>[];
    dummySearchList.addAll(duplicateItems);
    if(query.isNotEmpty) {
      List<String> dummyListData = <String>[];
      dummySearchList.forEach((item) {
        if(item.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        items.clear();
        items.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        items.clear();
        items.addAll(duplicateItems);
      });
    }
  }


  @override
  Widget build(BuildContext context) {

    double _width = MediaQuery.of(context).size.width * 1;
    double _height = MediaQuery.of(context).size.height * 1;

    return Scaffold(
      appBar: AppBar(
        title: titleAppbar,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context){
                      return AccountSettting(userName: userName,phoneNumber: userPhone,imgURL: imgURL,);
                    })
                );
              },
              icon: Icon(Icons.menu_rounded))

        ],
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
      ),
      body: LoadingOverlay(
          isLoading: _isLoading,
          color: Colors.white,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              child:  Column(
                children:<Widget> [
                  Container(
                      width: 70.0,
                      height: 70.0,
                      // ignore: unnecessary_new
                      child: ClipRRect(
                            child: imgURL == ""
                            ? Image.asset('assets/images/user.png',height: 40,)
                            : Image.network(imgURL,fit: BoxFit.fill,),
                          ),
                  ),
                  Container(
                    height: 50,
                  ),
                  Container(
                    height: _height - 260,
                    width: _width ,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                          child: Container(
                            height: 40,
                            child: TextField(
                              // onChanged: (value) {
                              //   filterSearchResults(value);
                              // },
                              controller: editingController,
                              decoration: const InputDecoration(
                                  contentPadding: EdgeInsets.all(10),
                                  hintText: "Tìm kiếm liên hệ",
                                  prefixIcon: Icon(Icons.search,color: Colors.black,),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(25.0)))),
                              onChanged: (writeText) {
                                if (mounted) {
                                  setState(() {
                                    _isLoading = true;
                                  });
                                }

                                if (mounted) {
                                  setState(() {
                                    _sortedAvailableUsers.clear();

                                    print('Available Users: $_availableUsers');

                                    _availableUsers.forEach((userNameMap) {
                                      if (userNameMap.values.first
                                          .toString()
                                          .toLowerCase()
                                          .startsWith(writeText.toLowerCase())) {
                                        _sortedAvailableUsers.add(userNameMap);
                                      }
                                    });
                                  });
                                }
                                print(_sortedAvailableUsers);

                                if (mounted) {
                                  setState(() {
                                    _isLoading = false;
                                  });
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: _height/50,
                        ),
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: _sortedAvailableUsers.length,
                            itemBuilder: (connectionContext, index) {
                              return connectionShowUp(index);
                            },
                          ),
                        ),
                      ],
                    ),
                    decoration: const BoxDecoration(
                      borderRadius:BorderRadius.only(
                          topLeft :Radius.circular(50),
                          topRight: Radius.circular(50)
                      ),
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          )),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  Widget _contactList(int index){
    return
      ListTile(
        onTap: (){},
        leading: CircleAvatar(
          child: CircleAvatar(
            foregroundColor: Colors.blue,
            backgroundColor: Colors.white,
            radius: 70.0,
            child: ClipOval(
              child: Image.network(
                'https://images.unsplash.com/photo-1582979512210-99b6a53386f9?ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
                fit: BoxFit.cover,
                width: 140.0,
                height: 140.0,
              ),
            ),
          ),
        ),
        title: Text(
          '${items[index]}',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
        ),
        trailing: Icon(Icons.phone_outlined,color: Colors.blue,size: 30,),
      );
  }

  ////////////////////////////////////////////////////////////////////////////////////

  Widget connectionShowUp(int index) {
    return Container(
      //height: 80.0,
      width: double.maxFinite,
      //color: Colors.orange,
      decoration: BoxDecoration(
        border: Border.all(
            color: Colors.lightBlueAccent,
            width: 4
        ),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Row (
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              width: 50.0,
              height: 50.0,
              // ignore: unnecessary_new
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  // ignore: unnecessary_new
                  image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(
                        _sortedAvailableUsers[index].values.first.toString().split('[user-name-about-divider]')[2] // 'https://images.unsplash.com/photo-1582979512210-99b6a53386f9?ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDN8fHxlbnwwfHx8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
                    ),
                  )
              )
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                _sortedAvailableUsers[index]
                    .values
                    .first
                    .toString()
                    .split('[user-name-about-divider]')[0],
                style: TextStyle(color: Colors.black, fontSize: 20.0),
              ),
              Text(
                _sortedAvailableUsers[index]
                    .values
                    .first
                    .toString()
                    .split('[user-name-about-divider]')[1],
                style: TextStyle(color: Colors.black, fontSize: 16.0),
              ),
            ],
          ),
          TextButton(
              style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0),
                    side: BorderSide(
                        color: _getRelevantButtonConfig(
                            connectionStateType:
                            ConnectionStateType.ButtonBorderColor,
                            index: index)),
                  )),
              child: _getRelevantButtonConfig(
                  connectionStateType: ConnectionStateType.ButtonNameWidget,
                  index: index),
              onPressed: () async {
                final String buttonName = _getRelevantButtonConfig(
                    connectionStateType: ConnectionStateType.ButtonOnlyName,
                    index: index);

                if (mounted) {
                  setState(() {
                    _isLoading = true;
                  });
                }

                if (buttonName == ConnectionStateName.Connect.toString()) {
                  if (mounted) {
                    setState(() {
                      _myConnectionRequestCollection.add({
                        _sortedAvailableUsers[index].keys.first.toString():
                        OtherConnectionStatus.Request_Pending.toString(),
                      });
                    });
                  }

                  await _cloudStoreDataManagement.changeConnectionStatus(
                      oppositeUserId:  _sortedAvailableUsers[index]
                          .keys
                          .first
                          .toString(),
                      currentUserId:
                      FirebaseAuth.instance.currentUser!.uid.toString(),
                      connectionUpdatedStatus:
                      OtherConnectionStatus.Invitation_Came.toString(),
                      currentUserUpdatedConnectionRequest:
                      _myConnectionRequestCollection);
                } else if (buttonName ==
                    ConnectionStateName.Accept.toString()) {
                  if (mounted) {
                    setState(() {
                      _myConnectionRequestCollection.forEach((element) {
                        if (element.keys.first.toString() ==
                            _sortedAvailableUsers[index]
                                .keys
                                .first
                                .toString()) {
                          _myConnectionRequestCollection[_myConnectionRequestCollection
                              .indexOf(element)] = {
                            _sortedAvailableUsers[index]
                                .keys
                                .first
                                .toString():
                            OtherConnectionStatus.Invitation_Accepted
                                .toString(),
                          };
                        }
                      });
                    });
                  }

                  await _cloudStoreDataManagement.changeConnectionStatus(
                      storeDataAlsoInConnections: true,
                      oppositeUserId: _sortedAvailableUsers[index]
                          .keys
                          .first
                          .toString(),
                      currentUserId:
                      FirebaseAuth.instance.currentUser!.uid.toString(),
                      connectionUpdatedStatus:
                      OtherConnectionStatus.Request_Accepted.toString(),
                      currentUserUpdatedConnectionRequest:
                      _myConnectionRequestCollection);
                }

                if (mounted) {
                  setState(() {
                    _isLoading = false;
                  });
                }
              }),
        ],
      ),
    );
  }

  dynamic _getRelevantButtonConfig(
      {required ConnectionStateType connectionStateType, required int index}) {
    bool _isUserPresent = false;
    String _storeStatus = '';

    _myConnectionRequestCollection.forEach((element) {
      if (element.keys.first.toString() ==
          _sortedAvailableUsers[index].keys.first.toString()) {
        _isUserPresent = true;
        _storeStatus = element.values.first.toString();
      }
    });

    if (_isUserPresent) {
      print('User Present in Connection List');

      if (_storeStatus == OtherConnectionStatus.Request_Pending.toString() ||
          _storeStatus == OtherConnectionStatus.Invitation_Came.toString()) {
        if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
          return Text(
            _storeStatus == OtherConnectionStatus.Request_Pending.toString()
                ? ConnectionStateName.Pending.toString()
                .split(".")[1]
                .toString()
                : ConnectionStateName.Accept.toString()
                .split(".")[1]
                .toString(),
            style: TextStyle(color: Colors.orange),
          );
        } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
          return _storeStatus ==
              OtherConnectionStatus.Request_Pending.toString()
              ? ConnectionStateName.Pending.toString()
              : ConnectionStateName.Accept.toString();
        }

        return Colors.orange;
      } else {
        if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
          return Text(
            ConnectionStateName.Connected.toString().split(".")[1].toString(),
            style: TextStyle(color: Colors.green),
          );
        } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
          return ConnectionStateName.Connected.toString();
        }

        return Colors.green;
      }
    } else {
      print('User Not Present in Connection List');

      if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
        return Text(
          ConnectionStateName.Connect.toString().split(".")[1].toString(),
          style: TextStyle(color: Colors.red),
        );
      } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
        return ConnectionStateName.Connect.toString();
      }

      return Colors.red;
    }
  }

}
