import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/services/facebook_auth.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/view/authentication/login.dart';
import 'package:fre_chat_app/view/chat/setting/infomation.dart';
import 'package:fre_chat_app/view/chat/setting/language.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

class AccountSettting extends StatefulWidget {
  const AccountSettting({ Key? key, required this.imgURL, required this.phoneNumber, required this.userName }) : super(key: key);
  final imgURL, phoneNumber, userName;
  @override
  _AccountSetttingState createState() => _AccountSetttingState();
}



class _AccountSetttingState extends State<AccountSettting>{

  final FacebookAuthentication _facebookAuthentication = FacebookAuthentication();
  final CloudFireStore _cloudFireStore = CloudFireStore();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  bool isSwitched = false;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('${widget.userName}  ${widget.imgURL} ${widget.phoneNumber}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(onPressed: () {
            Navigator.pop(context);
          }, icon: Icon(Icons.arrow_back)),
          title: const Text(""),
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).primaryColor,
          elevation: 0,
        ),
        body: Container(
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 70.0,
                        height: 70.0,
                        // ignore: unnecessary_new
                        child:  ClipRRect(
                          borderRadius: BorderRadius.circular(50.0),
                          child: widget.imgURL == ""
                          ? Image.asset('assets/images/user.png', height: 40,)
                          : Image.network(widget.imgURL,
                              height: 80.0,
                              width: 80.0,
                              fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(widget.userName, style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),),
                          // IconButton(onPressed: (){
                          //   showModalBottomSheet<void>(
                          //     context: context,
                          //     builder: (BuildContext context) {
                          //       return Column(
                          //         mainAxisAlignment: MainAxisAlignment.start,
                          //         children: [
                          //           Container(
                          //             padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                          //             child: TextField(
                          //               decoration: InputDecoration(
                          //                   border: OutlineInputBorder(
                          //                     borderRadius: BorderRadius.all(Radius.circular(10)),
                          //                   ),
                          //                   contentPadding: EdgeInsets.all(5)
                          //               ),
                          //               style: TextStyle(fontSize: 20),
                          //             ),
                          //           )
                          //         ],
                          //       );
                          //     },
                          //     backgroundColor: Colors.white,
                          //   );
                          // },
                          //   icon: const Icon(
                          //     Icons.drive_file_rename_outline_outlined,
                          //     color: Colors.white,
                          //   ),
                          // )
                        ],
                      ),
                      Text(widget.phoneNumber,style: const TextStyle(fontSize: 18, color: Colors.white)),
                      const Padding(padding: EdgeInsets.symmetric(vertical: 20)),
                      // InkWell(
                      //   child: Container(
                      //     decoration: const BoxDecoration(
                      //         color: Colors.white
                      //     ),
                      //     height: 50,
                      //     child: Row(
                      //       children:const <Widget>[
                      //         Padding(padding: EdgeInsets.symmetric(horizontal: 15)),
                      //         Expanded(child: Text("Ngôn ngữ", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 8,),
                      //         Expanded(child: Icon(Icons.arrow_forward_ios,color: Colors.black,size: 18,),flex: 1,)
                      //       ],
                      //     ),
                      //   ),
                      //   onTap: (){
                      //     Navigator.push(context, MaterialPageRoute(builder: (context){
                      //       return Language();
                      //     }));
                      //   },
                      // ),
                      const Padding(padding: EdgeInsets.only(top: 25)),
                      InkWell(
                          child: Container(
                            decoration: const BoxDecoration(
                              // border: Border(bottom: BorderSide(width: 0.5,color: Colors.black54)),
                                color: Colors.white
                            ),
                            height: 50,
                            child: Row(
                              children: <Widget>[
                                const Padding(padding: EdgeInsets.symmetric(horizontal: 15)),
                                const Expanded(child: Text("Chế độ tối", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 7,),
                                Expanded(child:
                                Switch(value: isSwitched,
                                  onChanged: (value){
                                    setState(() {
                                      isSwitched=value;
                                      print(isSwitched);
                                    });
                                  },
                                  activeTrackColor: Colors.lightGreenAccent,
                                  activeColor: Colors.green,
                                ),
                                )
                              ],
                            ),
                          )
                      ),
                      const Padding(padding: EdgeInsets.only(top: 25)),
                      InkWell(
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colors.white
                          ),
                          height: 50,
                          child: Row(
                            children:const <Widget>[
                              Padding(padding: EdgeInsets.symmetric(horizontal: 15)),
                              Expanded(child: Text("Thông tin ứng dụng", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 8,),
                              Expanded(child: Icon(Icons.arrow_forward_ios,color: Colors.black,size: 18,),flex: 1,)
                            ],
                          ),
                        ),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context){
                            return Infomation();
                          }));
                        },
                      ),
                      const Padding(padding: EdgeInsets.only(top: 25)),
                      Container(
                        width: double.infinity,
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.fromLTRB(25, 0, 0, 0),
                        decoration: const BoxDecoration(
                          // border: Border(top: BorderSide(width: 0.5,color: Colors.black54)),
                            color: Colors.white
                        ),
                        child: TextButton(
                          child: const  Text("Xóa tài khoản",style: TextStyle(color: Colors.red),),
                          onPressed: (){
                            print('xoá tài khoản');
                          },
                        ),
                      )
                    ]
                )
            )
        ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }
}