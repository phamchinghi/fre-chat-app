import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

class Infomation extends StatefulWidget {
  const Infomation({ Key? key }) : super(key: key);

  @override
  _InfomationState createState() => _InfomationState();
}

class _InfomationState extends State<Infomation>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        iconLeading: Icons.arrow_back,
        bottomRight: 0,
        bottomLeft: 0,
        title: 'Thông tin ứng dụng',
      ),
    );
  }
}