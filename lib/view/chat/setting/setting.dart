import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/view/chat/setting/media_saved.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

class SettingChat extends StatefulWidget {
  const SettingChat({ Key? key, this.userPhone, this.userName,this.avatar}) : super(key: key);
  final userPhone,userName,avatar;
  @override
  _SettingChatState createState() => _SettingChatState();
}

bool isSwitched = false;

class _SettingChatState extends State<SettingChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        iconLeading: Icons.arrow_back,
        bottomRight: 0,
        bottomLeft: 0,
        title: '',
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 70.0,
                  height: 70.0,
                  // ignore: unnecessary_new
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      // ignore: unnecessary_new
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: new NetworkImage(
                              'https://scontent-sjc3-1.xx.fbcdn.net/v/t1.6435-9/96793755_2568364003436818_2250665667441197056_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=174925&_nc_ohc=AhIj_wBdVtYAX-ZDw2M&_nc_ht=scontent-sjc3-1.xx&oh=e9d26ba64300379bd0c5630feb4d10ec&oe=61A13610'
                          )
                      )
                  )
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(widget.userName,
                    style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),
                  ),
                  IconButton(onPressed: (){
                    showModalBottomSheet<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                              child: TextField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                    ),
                                    contentPadding: EdgeInsets.all(5)
                                ),
                                style: TextStyle(fontSize: 20),
                              ),
                            )
                          ],

                        );
                      },
                      backgroundColor: Colors.white,
                    );
                  },
                    icon: const Icon(
                      Icons.drive_file_rename_outline_outlined,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
              Text("0971621634",style: const TextStyle(fontSize: 18, color: Colors.white)),
              const Padding(padding: EdgeInsets.symmetric(vertical: 20)),
              InkWell(
                child: Container(
                  decoration: const BoxDecoration(
                    // border: Border(bottom: BorderSide(width: 0.5,color: Colors.black54)),
                      color: Colors.white
                  ),
                  height: 50,
                  child: Row(
                    children:const <Widget>[
                      Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
                      Expanded(child: Icon(Icons.image_outlined, color: Colors.black,),flex: 1,),
                      Expanded(child: Text("Ảnh, video đã gửi", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 7,),
                      Expanded(child: Icon(Icons.arrow_forward_ios,color: Colors.black,size: 18,),flex: 1,)
                    ],
                  ),
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return MediaSaved();
                  }));
                },
              ),
              InkWell(
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Colors.white
                    ),
                    height: 50,
                    child: Row(
                      children: const <Widget>[
                        Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
                        Expanded(child: Icon(Icons.search_outlined, color: Colors.black,),flex: 1,),
                        Expanded(child: Text("Tìm kiếm trong cuộc trò chuyện", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 8,),
                      ],
                    ),
                  )
              ),
              const Padding(padding: EdgeInsets.only(top: 25)),
              InkWell(
                  child: Container(
                    decoration: const BoxDecoration(
                      // border: Border(bottom: BorderSide(width: 0.5,color: Colors.black54)),
                        color: Colors.white
                    ),
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        const Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
                        const Expanded(child: Icon(Icons.push_pin_outlined, color: Colors.black,),flex: 1,),
                        const Expanded(child: Text("Ghim", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 7,),
                        Expanded(child:
                        Switch(value: isSwitched,
                          onChanged: (value){
                            setState(() {
                              isSwitched=value;
                              print(isSwitched);
                            });
                          },
                          activeTrackColor: Colors.lightGreenAccent,
                          activeColor: Colors.green,
                        ),
                        )
                      ],
                    ),
                  )
              ),
              InkWell(
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white
                  ),
                  height: 50,
                  child: Row(
                    children:const <Widget>[
                      Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
                      Expanded(child: Icon(Icons.color_lens_outlined, color: Colors.black,),flex: 1,),
                      Expanded(child: Text("Giao diện", style: TextStyle(color: Colors.black,fontSize: 16)),flex: 8,),
                    ],
                  ),
                ),
                onTap: (){
                  showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(
                        height: 300,
                        child: GridView(
                            padding: EdgeInsets.only(top: 20),
                            children: [
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Đỏ');
                                },
                              ),
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.pink,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Hồng');
                                },
                              ),
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.yellow,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Vàng');
                                },
                              ),
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.deepPurple,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Tím');
                                },
                              ),
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.black,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Đen');
                                },
                              ),
                              InkWell(
                                child: Container(
                                    width: 50.0,
                                    height: 50.0,
                                    // ignore: unnecessary_new
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red,
                                        border: Border.all(width: 1,color: Colors.black54)
                                    )
                                ),
                                onTap: (){
                                  print('Bạn đã click màu Đỏ');
                                },
                              ),
                            ],
                            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 100,
                                crossAxisSpacing: 10,
                                childAspectRatio: 3/2,
                                mainAxisSpacing: 10)
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only( topLeft: Radius.circular(25), topRight: Radius.circular(25)),
                          color: Colors.white,
                        ),
                      );
                    },
                    backgroundColor: Colors.transparent,
                  );
                },
              ),
              const Padding(padding: EdgeInsets.only(top: 10)),
              Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  // border: Border(top: BorderSide(width: 0.5,color: Colors.black54)),
                    color: Colors.white
                ),
                child: TextButton(
                  child: const  Text("Xóa bạn",style: TextStyle(color: Colors.red),),
                  onPressed: (){
                    
                  },
                ),
              )
            ],
          ),
        )
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

}
