import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

class Language extends StatefulWidget {
  const Language({ Key? key }) : super(key: key);

  @override
  _LanguageState createState() => _LanguageState();
}

class _LanguageState extends State<Language>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(
        iconLeading: Icons.arrow_back,
        bottomRight: 0,
        bottomLeft: 0,
        title: 'Thay đổi ngôn ngữ',
      ),
    );
  }
}