import 'package:flutter/material.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';

class MediaSaved extends StatefulWidget {
  const MediaSaved({Key? key}) : super(key: key);

  @override
  _MediaSavedState createState() => _MediaSavedState();
}

class _MediaSavedState extends State<MediaSaved> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MyAppBar(
          title: "Ảnh, video đã gửi",
          bottomLeft: 0,
          bottomRight: 0,
          iconLeading: Icons.arrow_back,
        ),
        body: GridView(
          children: [
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
            Text('Hello'),
          ],
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 300,
              childAspectRatio: 3 / 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10),
        ));
  }
}
