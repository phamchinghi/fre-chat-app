import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:fre_chat_app/models/chat/user.dart';
import 'package:fre_chat_app/view/chat/addfriend.dart';
import 'package:fre_chat_app/view/chat/setting/media_saved.dart';

class MyFloatingButton extends StatefulWidget {
  const MyFloatingButton({Key? key}) : super(key: key);

  @override
  _MyFloatingButtonState createState() => _MyFloatingButtonState();
}


class _MyFloatingButtonState extends State<MyFloatingButton> {
  @override
  Widget build(BuildContext context) {
    List<Users> chatUsers = [
    Users(
        name: "Jane Russel",
        messageText: "Awesome Setup",
        imageURL:
        "https://images.unsplash.com/photo-1614632537190-23e4146777db?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8YmFsbHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
        time: "Now",
        status: true)];

    return SpeedDial(
      backgroundColor: Colors.white,
      activeBackgroundColor: Colors.redAccent,
      activeIcon: Icons.close,
      child: const  Icon(Icons.add),
      curve: Curves.bounceIn,
      closeManually: false,
      children: [
        SpeedDialChild(
          backgroundColor: Theme.of(context).primaryColor,
          child: const  Icon(Icons.phone_outlined, color: Colors.white,),
          label: 'Thêm bạn',
          labelStyle: TextStyle(color: Colors.black),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return AddFriend();
            }));
          }
        ),
        SpeedDialChild(
            backgroundColor: Theme.of(context).primaryColor,
            child: const  Icon(Icons.image_outlined, color: Colors.white,),
            label: 'Lịch sử media',
            labelStyle: TextStyle(color: Colors.black),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return MediaSaved();
              }));
            }
        )
      ],
    );
  }
}
