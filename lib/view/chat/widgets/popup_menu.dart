import 'package:flutter/material.dart';

class PopupMenu extends StatelessWidget {
  const PopupMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: Row(
        children: [
          Column(
            children: [
              IconButton(
                  icon: Icon(Icons.share_outlined),
                  onPressed: () {
                    print("Chia se");
                  }
              ),
              const Text('Chia sẻ', style: TextStyle(fontSize: 12,color: Colors.black54),),
            ],
          ),
        ],
      ),

    );
  }
}
