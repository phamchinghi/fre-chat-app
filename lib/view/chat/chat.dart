import 'dart:ffi';

import 'package:animations/animations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fre_chat_app/global_use/constants.dart';
import 'package:fre_chat_app/global_use/constants.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/models/chat/user.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/sqlite_management/local_database_management.dart';
import 'package:fre_chat_app/view/chat/chat_detail.dart';
import 'package:fre_chat_app/view/chat/conversation_list.dart';
import 'package:fre_chat_app/view/chat/setting/account_setting.dart';
import 'package:fre_chat_app/view/chat/user_pin_list.dart';
import 'package:loading_overlay/loading_overlay.dart';

class Chat extends StatefulWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> with WidgetsBindingObserver{
  Icon searchIcon = const Icon(Icons.search_rounded);
  Widget titleAppbar = const Text('');
 
  bool _isLoading = false;
  final List<String> _allUserConnectionActivity = ['Nghị'];
  final List<String> _allConnectionsUserName = [];
  final List<String> _allConnectionsUserId = [];
  final List<String> _allConnectionsUserAvt = [];
  final List<String> _allConnectionsUserPhone = [];
  final List<String> _allConnectionsUserStatus = [];

  String imgURL = "";
  String userPhone = "";
  String userName = "";

  final CloudFireStore _cloudStoreDataManagement = CloudFireStore();

  final LocalDatabase _localDatabase = LocalDatabase();

  static final FirestoreFieldConstants _firestoreFieldConstants = FirestoreFieldConstants();

  final FirebaseFirestore _cloudFirestore = FirebaseFirestore.instance;

  // final Future<DocumentSnapshot<Map<String, dynamic>>> getImage =  _cloudFirestore.collection('users').doc(FirebaseAuth.instance.currentUser!.uid).get();

  /// For New Connected User Data Entry
  Future<void> _checkingForNewConnection(QueryDocumentSnapshot<Map<String, dynamic>> queryDocumentSnapshot,List<QueryDocumentSnapshot<Map<String, dynamic>>> docs) async {
    if (mounted) {
      setState(() {
        this._isLoading = true;
      });
    }

    final List<dynamic> _connectionRequestList = queryDocumentSnapshot.get(_firestoreFieldConstants.connectionRequest);

    _connectionRequestList.forEach((connectionRequestData) {
      if (connectionRequestData.values.first.toString() ==
          OtherConnectionStatus.Invitation_Accepted.toString() ||
          connectionRequestData.values.first.toString() ==
              OtherConnectionStatus.Request_Accepted.toString()) {
        docs.forEach((everyDocument) async {
          if (everyDocument.id == connectionRequestData.keys.first.toString()) {
            final String _connectedUserName = everyDocument.get(_firestoreFieldConstants.userName);
            final String _token = everyDocument.get(_firestoreFieldConstants.token);
            final String _about = everyDocument.get(_firestoreFieldConstants.uid);
            final String _accCreationDate = everyDocument.get(_firestoreFieldConstants.createdDate);
            final String _accCreationTime = everyDocument.get(_firestoreFieldConstants.createdTime);
            final String _connectedUserId = everyDocument.get(_firestoreFieldConstants.uid);
            final String _connectedUserImgURL = everyDocument.get(_firestoreFieldConstants.imageURL);
            final String _connectedUserStatus = everyDocument.get(_firestoreFieldConstants.status);

            if (mounted) {
              setState(() {
                if (!this._allConnectionsUserId.contains(_connectedUserId)  
                // !this._allConnectionsUserName.contains(_connectedUserName)
                // !this._allConnectionsUserAvt.contains(_connectedUserImgURL) || 
                // !this._allConnectionsUserStatus.contains(_connectedUserStatus)
                ){
                  this._allConnectionsUserName.add(_connectedUserName);
                  this._allConnectionsUserId.add(_connectedUserId);
                  this._allConnectionsUserAvt.add(_connectedUserImgURL);
                  this._allConnectionsUserStatus.add(_connectedUserStatus);
                }
                 
              });
            }

            final bool _newConnectionUserNameInserted = await _localDatabase.insertOrUpdateDataForThisAccount(
                userName: _connectedUserName,
                userId: everyDocument.id,
                userToken: _token,
                userPhone: _about,
                userAccCreationDate: _accCreationDate,
                userAccCreationTime: _accCreationTime);

            if (_newConnectionUserNameInserted) {
              await _localDatabase.createTableForEveryUser(userId: _connectedUserId);
            }
          }
          print('connection id: ${_allConnectionsUserId}');
        });
      }
    });

    if (mounted) {
      setState(() {
        this._isLoading = false;
      });
    }
  }

  /// Fetch Real Time Data From Cloud Firestore
  Future<void> _fetchRealTimeDataFromCloudStorage() async {
    final realTimeSnapshot =
    await this._cloudStoreDataManagement.fetchRealTimeDataFromFirestore();

    realTimeSnapshot!.listen((querySnapshot) {
      querySnapshot.docs.forEach((queryDocumentSnapshot) async {
        if (queryDocumentSnapshot.id == FirebaseAuth.instance.currentUser!.uid.toString()) {
          await _checkingForNewConnection(queryDocumentSnapshot, querySnapshot.docs);
        }
      });
    });
  }



  @override
  void initState() {
    _fetchRealTimeDataFromCloudStorage();
    
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    setStatus('Online');
    getCurrentImgURL(FirebaseAuth.instance.currentUser!.uid);
    getCurrentUserPhone(FirebaseAuth.instance.currentUser!.uid);
    getCurrentUserName(FirebaseAuth.instance.currentUser!.uid);
  }

  void setStatus(String status) async{
    await _cloudFirestore.collection('users').doc(FirebaseAuth.instance.currentUser!.uid.toString()).update({'status':status,});
  }

  getCurrentImgURL(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    imgURL = documentSnapshot.get('imageURL').toString();
    print("img URL: "+imgURL);
  }
  getCurrentUserPhone(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    userPhone ="0" + documentSnapshot.get('phone_number').toString().substring(3,12);
    print("Phone number: "+userPhone);
  }
  getCurrentUserName(String uid)async{
    final DocumentSnapshot<Map<String, dynamic>> documentSnapshot =
      await FirebaseFirestore.instance
          .doc('users/$uid')
          .get();

    userName =documentSnapshot.get('user_name').toString();
    print("User name: "+userName);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);

    if(state == AppLifecycleState.resumed){
      //online
      setStatus('Online');
    }else{
      //offline
      setStatus('Offline');
    }
  }



  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width * 1;
    double _height = (MediaQuery.of(context).size.height * 1) - 285;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            setState(() {
              if (searchIcon.icon == Icons.search_rounded) {
                searchIcon = const Icon(Icons.cancel);
                titleAppbar = const ListTile(
                  title: TextField(
                    decoration: InputDecoration(
                      hintText: 'Tìm kiếm mọi người',
                      hintStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontStyle: FontStyle.italic,
                      ),
                      border: InputBorder.none,
                    ),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                );
              } else {
                searchIcon = const Icon(Icons.search_rounded);
                titleAppbar = const Text('');
              }
            });
          },
          icon: searchIcon),
        title: titleAppbar,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context){
                  return AccountSettting(userName: userName,phoneNumber: userPhone,imgURL: imgURL,);
                })
            );
          }, icon: Icon(Icons.menu_rounded)),

        ],
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0,
      ),
      body: LoadingOverlay(
          isLoading: _isLoading,
          color: Colors.white,
          child: SafeArea(
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: (){
                        print('Chuyển sang màn setting');
                      },
                      child:  ClipRRect(
                        borderRadius: BorderRadius.circular(50.0),
                        child: imgURL == ""
                        ? Image.asset('assets/images/user.png', height: 40)
                        : Image.network(imgURL,
                            height: 80.0,
                            width: 80.0,
                            fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    _activitiesList(context),
                    _connectionList(context)
                  ],
                ),
              ),
            ),
          )
      ),
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  Widget _activitiesList(BuildContext context){
    return Container(
      margin: const EdgeInsets.only(
        left: 10.0,
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).orientation == Orientation.portrait
              ? MediaQuery.of(context).size.height * (1.5 / 14)
              : MediaQuery.of(context).size.height * (3 / 14),
      child: ListView.builder(
        scrollDirection: Axis.horizontal, // Make ListView Horizontally
        itemCount: _allUserConnectionActivity.length,
        itemBuilder: (context, position) {
          return _activityCollectionList(context, position);
        },
      ),
    );
  }

  Widget _activityCollectionList(BuildContext context, int index) {
    return Container(
      margin: EdgeInsets.only(right: MediaQuery.of(context).size.width / 20),
      padding: EdgeInsets.only(top: 3.0),
      height: MediaQuery.of(context).size.height * (1.5 / 8),
      child: Column(
        children: [
          Stack(
            children: [
              if (_allUserConnectionActivity[index].contains('[[[new_activity]]]'))
                Container(
                    height:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? (MediaQuery.of(context).size.height * (1.2 / 7.95) /2.5) * 2
                        : (MediaQuery.of(context).size.height * (2.5 / 7.95) /2.5) * 2,
                    width:
                    MediaQuery.of(context).orientation == Orientation.portrait
                        ? (MediaQuery.of(context).size.height * (1.2 / 7.95) / 2.5) *  2
                        : (MediaQuery.of(context).size.height * (2.5 / 7.95) / 2.5) *  2,
                    child: CircularProgressIndicator(
                      color: Colors.blue,
                      value: 1.0,
                    ),
                  ),
                OpenContainer(
                  closedColor: Theme.of(context).primaryColor,
                  openColor: Theme.of(context).primaryColor,
                  middleColor: const Color.fromRGBO(34, 48, 60, 1),
                  closedElevation: 0.0,
                  closedShape: CircleBorder(),
                  transitionDuration: Duration(
                    milliseconds: 500,
                  ),
                  transitionType: ContainerTransitionType.fadeThrough,
                  openBuilder: (context, openWidget) {
                    return Center();
                  },
                  closedBuilder: (context, closeWidget) {
                    return CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage:
                      ExactAssetImage('assets/images/user.png'),
                      radius: MediaQuery.of(context).orientation ==
                          Orientation.portrait
                          ? MediaQuery.of(context).size.height * (1.2 / 16) / 2.5
                          : MediaQuery.of(context).size.height * (2.5 / 16) / 2.5,
                    );
                  },
                ),
                index == 0 // This is for current user Account
                ? Padding(padding: EdgeInsets.only(
                  top: MediaQuery.of(context).orientation == Orientation.portrait
                      ? MediaQuery.of(context).size.height * (0.7 / 16) - 5
                      : MediaQuery.of(context).size.height * (1.5 / 16) - 5,
                  left: MediaQuery.of(context).orientation == Orientation.portrait
                      ? MediaQuery.of(context).size.width / 3 - 95
                      : MediaQuery.of(context).size.width / 8 - 45,
                  ),
                  child: Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.lightBlue,
                      ),
                      child: GestureDetector(
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: MediaQuery.of(context).orientation == Orientation.portrait
                              ? MediaQuery.of(context).size.height * (1.3 / 16) / 2.5 * (3.5 / 6)
                              : MediaQuery.of(context).size.height * (1.3 / 16) / 2,
                        ),
                      )
                  ))
                : const SizedBox(),
            ],
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(
              top: 7.0,
            ),
            child: Text(
              _allUserConnectionActivity[index],
              style: TextStyle(
                color: Colors.white,
                fontSize: 12.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _connectionList(BuildContext context) {
    return LoadingOverlay(
      isLoading: _isLoading,
      child: Container(
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).orientation == Orientation.portrait
                ? 11.0
                : 0.0),
        padding: const EdgeInsets.only(top: 18.0, bottom: 10.0),
        height: MediaQuery.of(context).size.height * (5.15 / 8),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
              spreadRadius: 0.0,
              offset: const Offset(0.0, -5.0), // shadow direction: bottom right
            )
          ],
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
          border: Border.all(
            color: Colors.black26,
            width: 1.0,
          ),
        ),
        child:ListView.builder(
          // onReorder: (first, last) {
          //   // if (mounted) {
          //   //   setState(() {
          //   //     final String _draggableConnection =
          //   //     this._allConnectionsUserName.removeAt(first);
          //   //
          //   //     this._allConnectionsUserName.insert(
          //   //         last >= this._allConnectionsUserName.length
          //   //             ? this._allConnectionsUserName.length
          //   //             : last > first
          //   //             ? --last
          //   //             : last,
          //   //         _draggableConnection);
          //   //   });
          //   // }
          // },
          itemCount: _allConnectionsUserName.length,
          itemBuilder: (context, position) {
            return Slidable(
              endActionPane: ActionPane(
                // A motion is a widget used to control how the pane animates.
                motion: const DrawerMotion(),

                // All actions are defined in the children parameter.
                children: [
                  // A SlidableAction can have an icon and/or a label.
                  SlidableAction(
                    onPressed: (context){
                      print(context);
                    },
                    backgroundColor: Color(0xFFFE4A49),
                    foregroundColor: Colors.white,
                    icon: Icons.delete,
                    label: 'Xóa',
                  ),
                  SlidableAction(
                    onPressed: (context){
                      print(context);
                    },
                    backgroundColor: Theme.of(context).primaryColor,
                    foregroundColor: Colors.white,
                    icon: Icons.push_pin_outlined,
                    label: 'Ghim',
                  ),
                ],
              ),
              child: chatTileContainer(context, position, _allConnectionsUserName[position],_allConnectionsUserId[position],_allConnectionsUserAvt[position],_allConnectionsUserStatus[position]),
            );
          },
        ),
      ),
    );
  }

  Widget chatTileContainer(BuildContext context, int index, String _userName, String userId,String imgURL, String status) {
  return GestureDetector(
    key: Key('$index'),
    child: Container(
      padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
              elevation: 0.0,
              primary: Theme.of(context).accentColor,
              onPrimary: Theme.of(context).accentColor,
            ),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return ChatDetail(userName: _userName,uid: userId, imgURL: imgURL);
          }));
        },
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 5,bottom: 5
                    ),
                    child: OpenContainer(
                      closedColor: Theme.of(context).primaryColor,
                      openColor: Theme.of(context).primaryColor,
                      middleColor: const Color.fromRGBO(31, 51, 71, 1),
                      closedShape: CircleBorder(),
                      closedElevation: 0.0,
                      transitionDuration: Duration(milliseconds: 500),
                      transitionType: ContainerTransitionType.fadeThrough,
                      openBuilder: (_, __) {
                        return Center(
                          child: Text('Story ở đây nha')
                        );
                      },
                      closedBuilder: (_, __) {
                        return CircleAvatar(
                          radius: 30.0,
                          backgroundColor: Colors.white,
                          child: ClipRRect(
                            child: imgURL == ""
                            ? Image.asset('assets/images/user.png')
                            : Image.network(imgURL,fit: BoxFit.fill,),
                          ),
                          // getProperImageProviderForConnectionsCollection(
                          //    _userName),
                        );
                      },
                    ),
                  ),
                  SizedBox(width: 16,),
                  OpenContainer(
                    closedColor: Colors.white,
                    openColor: Colors.white,
                    middleColor:Colors.lightBlueAccent[100],
                    closedElevation: 0.0,
                    openElevation: 0.0,
                    transitionDuration: Duration(milliseconds: 500),
                    transitionType: ContainerTransitionType.fadeThrough,
                    openBuilder: (context, openWidget) {
                      return ChatDetail(userName: _userName, uid: userId,imgURL: imgURL);
                    },
                    closedBuilder: (context, closeWidget) {
                      return Container(
                        alignment: Alignment.centerLeft,
                        width: MediaQuery.of(context).size.width / 2 + 30,
                        padding: EdgeInsets.only(
                          top: 5.0,
                          bottom: 5.0,),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              _userName.length <= 18
                                  ? _userName
                                  : '${_userName.replaceRange(18, _userName.length, '...')}',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              height: 12.0,
                            ),

                            /// For Extract latest Conversation Message
  //                          _latestDataForConnectionExtractPerfectly(_userName)
                            Text(
                              'Hello Nghi',
                              style: TextStyle(color: Colors.black26),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Text("06:00",
                  // style: TextStyle(fontSize: 12,fontWeight: widget.isMessageRead?FontWeight.bold:FontWeight.normal),
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54
                  ),
                ),
                status == "Online"
                ? Text('on',
                  style: TextStyle(
                    fontSize: 11,
                    color:  Colors.green,
                  ),)
                : Text('off',style: TextStyle(
                    fontSize: 11,
                    color:  Colors.red,
                  ))
                // StreamBuilder<DocumentSnapshot>(
                //       stream: FirebaseFirestore.instance.collection('users').doc().snapshots(),
                //       builder: (context, snapshot) {
                //         final status = snapshot.data;
                //         if(status != null){
                //             return Text(status['status'].toString(),
                //             style: TextStyle(
                //               fontSize: 12, 
                //               fontWeight: FontWeight.normal,
                //               color: Colors.green
                //             )
                //           );
                        
                //       }
                //       return Text('....',
                //             style: TextStyle(
                //               fontSize: 12, 
                //               fontWeight: FontWeight.normal,
                //               color: Colors.green
                //             )
                //           );
                //       }
                //     )
              ],
            )
          ],
        ),
      ),
    ),
  );
  }

}
