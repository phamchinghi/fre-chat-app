import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fre_chat_app/global_use/enum_gen.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';
import 'package:fre_chat_app/widgets/myappbar.dart';
import 'package:loading_overlay/loading_overlay.dart';
class AddFriend extends StatefulWidget {
  const AddFriend({Key? key}) : super(key: key);

  @override
  _AddFriendState createState() => _AddFriendState();
}

class _AddFriendState extends State<AddFriend> {

  Map<String, dynamic>? userMap;
  final TextEditingController _search = TextEditingController();

  
  void onSearch() async{
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    setState(() {
      _isLoading = true;
    });
    await _firestore.collection('users')
        .where("phone_number", isEqualTo: _search.text.trim())
        .get()
        .then((value){
      setState(() {
        userMap = value.docs[0].data();
        _isLoading = false;
      });
      print(userMap);
    });
  }

  List<Map<String, dynamic>> _availableUsers = [];
  List<Map<String, dynamic>> _sortedAvailableUsers = [];
  List<dynamic> _myConnectionRequestCollection = [];

  bool _isLoading = false;

  final CloudFireStore _cloudStoreDataManagement = CloudFireStore();

  Future<void> _initialDataFetchAndCheckUp() async {
    if (mounted) {
      setState(() {
        _isLoading = true;
      });
    }

    final List<Map<String, dynamic>> takeUsers = await _cloudStoreDataManagement.getAllUsersListExceptMyAccount(currentUser: FirebaseAuth.instance.currentUser!.uid.toString());

    final List<Map<String, dynamic>> takeUsersAfterSorted = [];

    // if (mounted) {
    //   setState(() {
    //     takeUsers.forEach((element) {
    //       if (mounted) {
    //         setState(() {
    //           takeUsersAfterSorted.add(element);
    //         });
    //       }
    //     });
    //   });
    // }

    final List<dynamic> _connectionRequestList =
    await _cloudStoreDataManagement.currentUserConnectionRequestList(
        userId: FirebaseAuth.instance.currentUser!.uid.toString());

    if (mounted) {
      setState(() {
        _availableUsers = takeUsers;
        _sortedAvailableUsers = takeUsersAfterSorted;
        _myConnectionRequestCollection = _connectionRequestList;
      });
    }

    if (mounted) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  void initState() {
    _initialDataFetchAndCheckUp();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: MyAppBar(title: 'Thêm bạn',bottomLeft: 0,bottomRight: 0,iconLeading: Icons.arrow_back,),
        backgroundColor: const Color.fromRGBO(213, 219, 229, 1.0),
        body: LoadingOverlay(
          isLoading: _isLoading,
          color: Colors.black54,
          child: Container(
            margin: EdgeInsets.all(12.0),
            width: double.maxFinite,
            height: double.maxFinite,
            child: ListView(
              shrinkWrap: true,
              children: [
                Container(
                  width: double.maxFinite,
                  margin: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: TextField(
                     controller: _search,
                    //autofocus: true,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      hintText: 'Tìm kiếm bạn bè',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(27),
                      ),
                      //prefixText: '+84',
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.lightBlueAccent, width: 3.0),
                      ),
                      hintStyle: TextStyle(color: Colors.cyan,fontWeight: FontWeight.w400,fontStyle: FontStyle.italic),
                      // focusedBorder: UnderlineInputBorder(
                      //     borderSide:
                      //     BorderSide(width: 2.0, color: Colors.lightBlue)),
                      // enabledBorder: UnderlineInputBorder(
                      //     borderSide:
                      //     BorderSide(width: 2.0, color: Colors.lightBlue)),
                    ),
                    onChanged: (writeText) {
                      if (mounted) {
                        setState(() {
                           _isLoading = true;
                        });
                      }

                      if (mounted) {
                        setState(() {
                         _sortedAvailableUsers.clear();

                          print('Available Users: $_availableUsers');

                          _availableUsers.forEach((userNameMap) {
                            if (userNameMap.values.first
                                .toString()
                                .toLowerCase()
                                .startsWith(writeText.toLowerCase())) {
                              _sortedAvailableUsers.add(userNameMap);
                            }
                          });
                        });
                      }
                      print(_sortedAvailableUsers);

                      if (mounted) {
                        setState(() {
                          _isLoading = false;
                        });
                      }
                    },
                  ),
                ),
                // ElevatedButton(
                //   onPressed: onSearch,
                //   child: Text('Tìm Kiếm'),
                // ),
                // userMap != null
                //     ? ListTile(
                //   onTap: (){},
                //   leading: Icon(Icons.account_circle_outlined, color:Colors.black),
                //   title: Text(
                //       userMap!['user_name'],
                //     style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.w500),
                //   ),
                //   subtitle: Text(userMap!['phone_number']),
                // ) :
                Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  height: MediaQuery.of(context).size.height - 50,
                  width: double.maxFinite,
                  //color: Colors.red,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: _sortedAvailableUsers.length,
                    itemBuilder: (connectionContext, index) {
                      return connectionShowUp(index);
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget connectionShowUp(int index) {
    return Container(
      height: 80.0,
      width: double.maxFinite,
      //color: Colors.orange,
        decoration: BoxDecoration(
            border: Border.all(
              color: Colors.white60,
              width: 3
            ),
            borderRadius: BorderRadius.circular(15.0),
          ),
      child: Row (
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(child: CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.white,
            child: ClipRRect(
              child: _sortedAvailableUsers[index].values.first.toString().split('[user-name-about-divider]')[2].isEmpty
              ? Image.asset('assets/images/user.png')
              : Image.network(_sortedAvailableUsers[index].values.first.toString().split('[user-name-about-divider]')[2],fit: BoxFit.fill,),
            ),
            // getProperImageProviderForConnectionsCollection(
            //    _userName),
          )),
          Expanded(child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                _sortedAvailableUsers[index]
                    .values
                    .first
                    .toString()
                    .split('[user-name-about-divider]')[0],
                style: TextStyle(color: Colors.black, fontSize: 20.0),
              ),
              Text(
                _sortedAvailableUsers[index]
                    .values
                    .first
                    .toString()
                    .split('[user-name-about-divider]')[1],
                style: TextStyle(color: Colors.black, fontSize: 16.0),
              ),
            ],
          ),),
          Expanded(child: TextButton(
              style: TextButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0),
                    side: BorderSide(
                        color: _getRelevantButtonConfig(
                            connectionStateType:
                            ConnectionStateType.ButtonBorderColor,
                            index: index)),
                  )),
              child: _getRelevantButtonConfig(
                  connectionStateType: ConnectionStateType.ButtonNameWidget,
                  index: index),
              onPressed: () async {
                final String buttonName = _getRelevantButtonConfig(
                    connectionStateType: ConnectionStateType.ButtonOnlyName,
                    index: index);

                if (mounted) {
                  setState(() {
                  _isLoading = true;
                  });
                }

                if (buttonName == ConnectionStateName.Connect.toString()) {
                  if (mounted) {
                    setState(() {
                      _myConnectionRequestCollection.add({
                        _sortedAvailableUsers[index].keys.first.toString():
                        OtherConnectionStatus.Request_Pending.toString(),
                      });
                    });
                  }

                  await _cloudStoreDataManagement.changeConnectionStatus(
                      oppositeUserId:  _sortedAvailableUsers[index]
                          .keys
                          .first
                          .toString(),
                      currentUserId:FirebaseAuth.instance.currentUser!.uid.toString(),
                      connectionUpdatedStatus:OtherConnectionStatus.Invitation_Came.toString(),
                      currentUserUpdatedConnectionRequest:_myConnectionRequestCollection);
                } else if (buttonName ==
                    ConnectionStateName.Accept.toString()) {
                  if (mounted) {
                    setState(() {
                      _myConnectionRequestCollection.forEach((element) {
                        if (element.keys.first.toString() ==
                            _sortedAvailableUsers[index]
                                .keys
                                .first
                                .toString()) {
                          _myConnectionRequestCollection[_myConnectionRequestCollection
                              .indexOf(element)] = {
                            _sortedAvailableUsers[index]
                                .keys
                                .first
                                .toString():
                            OtherConnectionStatus.Invitation_Accepted.toString(),
                          };
                        }
                      });
                    });
                  }

                  await _cloudStoreDataManagement.changeConnectionStatus(
                      storeDataAlsoInConnections: true,
                      oppositeUserId: _sortedAvailableUsers[index]
                          .keys
                          .first
                          .toString(),
                      currentUserId:
                      FirebaseAuth.instance.currentUser!.uid.toString(),
                      connectionUpdatedStatus:
                      OtherConnectionStatus.Request_Accepted.toString(),
                      currentUserUpdatedConnectionRequest:
                      _myConnectionRequestCollection);
                }

                if (mounted) {
                  setState(() {
                    _isLoading = false;
                  });
                }
              }))
        ],
      ),
    );
  }

  dynamic _getRelevantButtonConfig({required ConnectionStateType connectionStateType, required int index}) {
    bool _isUserPresent = false;
    String _storeStatus = '';

    _myConnectionRequestCollection.forEach((element) {
      if (element.keys.first.toString() ==
          _sortedAvailableUsers[index].keys.first.toString()) {
        _isUserPresent = true;
        _storeStatus = element.values.first.toString();
      }
    });

    if (_isUserPresent) {
      print('User Present in Connection List');

      if (_storeStatus == OtherConnectionStatus.Request_Pending.toString() ||
          _storeStatus == OtherConnectionStatus.Invitation_Came.toString()) {
        if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
          return Text(
            _storeStatus == OtherConnectionStatus.Request_Pending.toString()
                ? "Đã gửi yêu cầu"
                // .split(".")[1]
                // .toString()
                : "Đồng ý",
                // .split(".")[1]
                // .toString(),
            style: TextStyle(color: Colors.orange),
          );
        } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
          return _storeStatus ==
              OtherConnectionStatus.Request_Pending.toString()
              ? "Đã gửi yêu cầu"
              : "Đồng ý";
        }

        return Colors.orange;
      } else {
        if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
          return Text(
            // ConnectionStateName.Connected.toString().split(".")[1].toString(),
            "Đã kết nối",
            style: TextStyle(color: Colors.green),
          );
        } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
          return "Đã kết nối";
        }

        return Colors.green;
      }
    } else {
      print('User Not Present in Connection List');

      if (connectionStateType == ConnectionStateType.ButtonNameWidget) {
        return Text(
          // ConnectionStateName.Connect.toString().split(".")[1].toString(),
          "Kết bạn",
          style: TextStyle(color: Colors.red),
        );
      } else if (connectionStateType == ConnectionStateType.ButtonOnlyName) {
        return ConnectionStateName.Connect.toString();
      }

      return Colors.red;
    }
  }

}
