import 'package:flutter/material.dart';
import 'package:fre_chat_app/view/chat/chat_detail.dart';
import 'chat_detail.dart';

class UserPinList extends StatefulWidget {
  
  String imageURL;

  UserPinList({required this.imageURL});

  @override
  _UserPinListState createState() => _UserPinListState();
}

class _UserPinListState extends State<UserPinList> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){},
      child: Container(
        width: 60.0,
        height: 60.0,
        // ignore: unnecessary_new
        decoration: new BoxDecoration(
            shape: BoxShape.circle,
            // ignore: unnecessary_new
            image: new DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(widget.imageURL)
            )
        )
      ),
    );
  }
}