import 'package:flutter/material.dart';
import 'package:fre_chat_app/models/chat/user.dart';

class FindUserList extends StatefulWidget {
  String name;
  String imgUrl;
  String phoneNumber;


  FindUserList(this.name, this.imgUrl, this.phoneNumber);

  @override
  _FindUserListState createState() => _FindUserListState();
}

class _FindUserListState extends State<FindUserList> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        print('Click vào người đã tìm kiếm');
      },
      child: Card(
        // padding: EdgeInsets.only(left: 16,right: 16,top: 10,bottom: 10),
        elevation: 5,
        color: Colors.blue[100],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25)
        ),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 10,bottom: 10),),
              Expanded(
                child: Row(
                  children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 10)),
                    CircleAvatar(
                      backgroundImage: NetworkImage(widget.imgUrl),
                      maxRadius: 25,
                    ),
                    SizedBox(width: 16,),
                    Expanded(
                      child: Container(
                        color: Colors.transparent,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(widget.name, style: TextStyle(fontSize: 16),),
                            SizedBox(height: 6,),
                            Text(widget.phoneNumber,style: TextStyle(fontSize: 13,color: Colors.grey.shade600),),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
