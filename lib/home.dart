import 'package:flutter/material.dart';
import 'package:fre_chat_app/view/chat/chat.dart';
import 'package:fre_chat_app/view/chat/contact.dart';

import 'view/chat/widgets/floating_button.dart';



class Home extends StatefulWidget{

  @override
  _HomeState createState() => _HomeState();

}

class _HomeState extends State<Home>{

  int currentTab = 0;
  final List<Widget> screens = [Chat(),Contact()];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Chat();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: PageStorage(
          bucket: bucket,
          child: currentScreen
      ),
      floatingActionButton: Container(
        height: 50,
        child: MyFloatingButton()
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            // mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: InkWell(
                    onTap: (){
                      setState(() {
                        currentScreen = Chat();
                        currentTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.chat_rounded,
                          color: currentTab == 0 ? Theme.of(context).primaryColor : Colors.blueGrey,
                        ),
                        Text(
                            'Chat',
                            style: TextStyle(color: currentTab == 0 ? Theme.of(context).primaryColor : Colors.blueGrey)
                        )
                      ],
                    ),
                  )
                )
              ),
              Expanded(
                child: Container(
                  child: InkWell(
                    onTap: (){
                      setState(() {
                            currentScreen = Contact();
                            currentTab = 1;
                          });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.phone,
                          color: currentTab == 1 ? Theme.of(context).primaryColor : Colors.blueGrey,
                        ),
                        Text(
                            'Liên hệ',
                            style: TextStyle(color: currentTab == 1 ? Theme.of(context).primaryColor : Colors.blueGrey)
                        )
                      ],
                    ),
                  )
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}