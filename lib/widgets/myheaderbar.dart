import 'package:flutter/material.dart';

class MyHeaderBar extends StatelessWidget {

  final IconData icon;
  
  const MyHeaderBar({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      decoration:const BoxDecoration(
        borderRadius:BorderRadius.only(
          bottomRight :Radius.circular(200)
        ),
        color: Color(0xFF00C9FF),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon,size: 96,color: Colors.white,)
        ],
      ),
    );
  }
}