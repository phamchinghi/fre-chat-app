import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget with PreferredSizeWidget{
  final String title;
  final IconData? iconLeading;
  final double? bottomLeft;
  final double? bottomRight;
  final Color? color;
  MyAppBar({
    Key? key,
    this.bottomLeft,
    this.bottomRight,
    this.iconLeading,
    required this.title,
    this.color
  }): super(key: key);

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(40);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: Icon(iconLeading, color: color,),
            onPressed: () {
              // Scaffold.of(context).openDrawer();
              Navigator.pop(context);
              },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            alignment: Alignment.centerLeft,
          );
        },
      ),
      title: Text(title),
      centerTitle: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(bottomLeft!),
          bottomRight: Radius.circular(bottomRight!)
        ),
      ),
      toolbarHeight: 60,
      backgroundColor: (color == null) ? Theme.of(context).primaryColor : color,
    );
  }
}
