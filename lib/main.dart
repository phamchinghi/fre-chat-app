import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:fre_chat_app/global_use/foreground_noti_management.dart';
import 'package:fre_chat_app/home.dart';
import 'package:fre_chat_app/services/firestore_manage.dart';

import 'view/authentication/login.dart';



Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  //init noti setting
  await notificationInit();

  //background message handle
  FirebaseMessaging.onBackgroundMessage(bgMessageAction);

  //Foreground message handle
  FirebaseMessaging.onMessage.listen((messageEvent){
    print('message data is: ${messageEvent.notification!.title} ${messageEvent.notification!.body}');
  
    _receiveAndshowNoti(title: messageEvent.notification!.title.toString(), body: messageEvent.notification!.body.toString());
  });

  runApp(MaterialApp(
      title: 'FRE Chat app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: const Color(0xFF00C9FF),
        accentColor: const Color(0xFFFFFFFF),
      ),
      home: await checkCurrentUserNull(),
      debugShowCheckedModeBanner: false,
      )
  );
}


Future<Widget> checkCurrentUserNull() async {
    if(FirebaseAuth.instance.currentUser == null ){
      return AnimatedSplashScreen(
        duration: 3000,
        splash: Image.asset(
          'assets/images/app_icon.png',
        ),
        splashIconSize: 144,
        splashTransition: SplashTransition.fadeTransition,
        backgroundColor: Color(0xFF00C9FF),
        nextScreen: Login(),
      );
    }else{
      final CloudFireStore _cloudFireStore =  CloudFireStore();
      
      final bool _dataResponse = await _cloudFireStore.userRecordPresentOrNot(
        userId: FirebaseAuth.instance.currentUser!.uid.toString());

      return _dataResponse 
      ? AnimatedSplashScreen(
        duration: 3000,
        splash: Image.asset(
          'assets/images/app_icon.png',
        ),
        splashIconSize: 144,
        splashTransition: SplashTransition.fadeTransition,
        backgroundColor: const Color(0xFF00C9FF),
        nextScreen: Home()) 
      : AnimatedSplashScreen(
        duration: 3000,
        splash: Image.asset(
          'assets/images/app_icon.png',
        ),
        splashIconSize: 144,
        splashTransition: SplashTransition.fadeTransition,
        backgroundColor: const Color(0xFF00C9FF),
        nextScreen: Login(),
      );

    }
  }

  Future<void> notificationInit()async{
    await FirebaseMessaging.instance.subscribeToTopic("FRE_ChatApp");

    //foreground noti option anable

    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

//receive and show message

void _receiveAndshowNoti({required String title, required String body})async{
  final ForegroundNotificationManagement _foreNotification = ForegroundNotificationManagement();

  print("Notification avtived");

  await _foreNotification.shownotification(title: title, body: body);
}

Future<void> bgMessageAction(RemoteMessage message)async {
  await Firebase.initializeApp();

  _receiveAndshowNoti(title: message.notification!.title.toString(), body: message.notification!.body.toString());
}
